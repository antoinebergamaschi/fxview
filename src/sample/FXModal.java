package sample;

import javafx.animation.FadeTransition;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

/**
 * Created by antoine bergamaschi on 02/04/2015.
 */
public class FXModal extends Stage {

    private Node modalContent;
    private Node modalBottom;
    private Node modalTop;

    private ImageView iconTitle;
    private Label labelTitle;

    private double modalDimX=2000;
    private double modalDimY=2000;

    public FXModal(){
        this(200, 200);
    }

    public FXModal(double initDimX, double initDimY){
        super(StageStyle.TRANSPARENT);
        this.modalDimX = initDimX;
        this.modalDimY = initDimY;

        AnchorPane root = build();

        this.setMinHeight(modalDimY);
        this.setMinWidth(modalDimX);
//        this.setResizable(true);

        ModalScene scene  = new ModalScene(root);
        scene.initListener(this);
        scene.setFill(Color.TRANSPARENT);
        this.setScene(scene);

        scene.getStylesheets().add(FXutils.getRessource("FXcss.default"));
        this.sizeToScene();
    }

    private AnchorPane build(){
        AnchorPane root = new AnchorPane();
        BorderPane pane = new BorderPane();
        root.getStyleClass().add("contentModal");


        root.setPrefSize(modalDimX, modalDimY);
        AnchorPane.setBottomAnchor(pane, 0.0d);
        AnchorPane.setTopAnchor(pane, 0.0d);
        AnchorPane.setRightAnchor(pane, 0.0d);
        AnchorPane.setLeftAnchor(pane, 0.0d);
        root.getChildren().add(pane);

        //Init Button
        FXButton  close_fxButton = new FXButton(FXButton.IconeButtonType.CLOSE,"",FXutils.getRessource("FXButton.tooltip.close"));
        FXButton mini_fxButton = new FXButton(FXButton.IconeButtonType.ICONIFY,"",FXutils.getRessource("FXButton.tooltip.iconified"));
        FXButton full_fxButton = new FXButton(FXButton.IconeButtonType.FULLSCREEN,"",FXutils.getRessource("FXButton.tooltip.fullscreen"));

        //Init Title and Icon
        labelTitle = new Label("test titre");
        labelTitle.getStyleClass().add("titleModal");

        iconTitle = new ImageView(new Image(FXutils.getRessource("logo.default")));

        iconTitle.setFitHeight(25);
        iconTitle.setPreserveRatio(true);
        iconTitle.setSmooth(true);

        this.getIcons().clear();
        this.getIcons().add(iconTitle.getImage());

        HBox title = new HBox();
        title.setAlignment(Pos.CENTER_LEFT);
        title.getChildren().addAll(iconTitle,labelTitle);


        HBox hBox = new HBox();
        hBox.getChildren().addAll(mini_fxButton, full_fxButton, close_fxButton);
        hBox.setAlignment(Pos.CENTER_RIGHT);

        modalTop =  new AnchorPane();
//        ((AnchorPane)modalTop).setPrefSize(0,30);
        AnchorPane.setRightAnchor(hBox, 0.0d);
        AnchorPane.setTopAnchor(hBox, 0.0d);
        AnchorPane.setBottomAnchor(hBox, 0.0d);


        AnchorPane.setLeftAnchor(title, 0.0d);
        AnchorPane.setTopAnchor(title, 0.0d);
        AnchorPane.setBottomAnchor(title, 0.0d);

//        HBox top = new HBox();
//        top.setAlignment(Pos.CENTER);
//        top.setMinSize(Region.USE_COMPUTED_SIZE,Region.USE_COMPUTED_SIZE);
        ((AnchorPane)modalTop).getChildren().addAll(title, hBox);

        pane.setTop(modalTop);


        close_fxButton.setOnAction(l -> {
            FadeTransition fadeTransition = new FadeTransition(new Duration(500), root);
            fadeTransition.setByValue(1);
            fadeTransition.setToValue(0);


            fadeTransition.setOnFinished(k -> {
                this.close();
            });

            //Fire custom action ?
            fadeTransition.playFromStart();
        });


        mini_fxButton.setOnAction(l -> {
            this.setIconified(true);
        });
        
        full_fxButton.setOnAction(l -> {
            if ( !this.isFullScreen() ) {
                FadeTransition fadeTransition = new FadeTransition(new Duration(250), root);
                fadeTransition.setByValue(1);
                fadeTransition.setToValue(0);

                fadeTransition.setOnFinished(k -> {
                    this.setFullScreen(true);
                    pane.setPrefSize(this.getWidth(), this.getHeight());

                    FadeTransition fadeEnd = new FadeTransition(new Duration(500), root);

                    fadeEnd.setByValue(0);
                    fadeEnd.setToValue(1);

                    fadeEnd.playFromStart();
                });

                //Fire custom action ?
                fadeTransition.playFromStart();
            }else{
                FadeTransition fadeTransition = new FadeTransition(new Duration(250), root);
                fadeTransition.setByValue(1);
                fadeTransition.setToValue(0);

                fadeTransition.setOnFinished(k -> {
                    this.setFullScreen(false);
                    pane.setPrefSize(this.getWidth(), this.getHeight());

                    FadeTransition fadeEnd = new FadeTransition(new Duration(500), root);

                    fadeEnd.setByValue(0);
                    fadeEnd.setToValue(1);

                    fadeEnd.playFromStart();
                });

                //Fire custom action ?
                fadeTransition.playFromStart();
            }
        });


        modalContent =  new AnchorPane();
        modalContent.getStyleClass().add("bodyModal");
        AnchorPane anchorTop  = new AnchorPane();
        anchorTop.setPrefSize(0, 10);
        AnchorPane.setBottomAnchor(modalContent, 0.0d);
        AnchorPane.setLeftAnchor(modalContent, 0.0d);
        AnchorPane.setTopAnchor(modalContent, 0.0d);
        AnchorPane.setRightAnchor(modalContent, 0.0d);
        anchorTop.getChildren().add(modalContent);


        modalBottom  =  new AnchorPane();
        AnchorPane anchorBottom  = new AnchorPane();
        anchorBottom.setPrefSize(0, 10);
        AnchorPane.setBottomAnchor(modalBottom, 0.0d);
        AnchorPane.setLeftAnchor(modalBottom, 0.0d);
        AnchorPane.setTopAnchor(modalBottom, 0.0d);
        AnchorPane.setRightAnchor(modalBottom, 0.0d);
        anchorBottom.getChildren().add(modalBottom);

        modalBottom.getStyleClass().add("bottomModal");

        pane.setCenter(anchorTop);
        pane.setBottom(anchorBottom);


        modalContent.setOnMousePressed(e -> {
            //Destroy event
            e.consume();
        });

        modalContent.setOnMouseDragged(e->{
            //Destroy event
            e.consume();
        });

        return root;
    }

    public void setModalTitle(String title){
        setModalTitle(title,null);
    }

    public void setModalTitle(String title, Image icon){
        //Init Title on the stage
        this.getIcons().clear();
        this.setTitle("");

        if ( icon != null ) {
            this.getIcons().add(icon);
            this.iconTitle.setImage(icon);
        }
        else{
            this.iconTitle.setImage(null);
        }

        if ( title != null ) {
            this.setTitle(title);
            //Add title in the ModalScene
            this.labelTitle.setText(title);
        }else{
            this.labelTitle.setText("");
        }
    }

    public void setBody(Node node){
        //Empty the current ModalBody
        ((AnchorPane)modalContent).getChildren().clear();
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setFitToWidth(true);
        scrollPane.setFitToHeight(true);
        scrollPane.setClip(node.getClip());

        AnchorPane.setBottomAnchor(scrollPane, 0.0d);
        AnchorPane.setLeftAnchor(scrollPane, 0.0d);
        AnchorPane.setTopAnchor(scrollPane, 0.0d);
        AnchorPane.setRightAnchor(scrollPane, 0.0d);

        ((AnchorPane) modalContent).getChildren().add(scrollPane);

        scrollPane.setContent(node);
    }
}
