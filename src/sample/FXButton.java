package sample;

import javafx.geometry.Rectangle2D;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by antoine bergamaschi on 02/04/2015.
 */
public class FXButton extends Button{
    private ImageView imageView = new ImageView();

    public enum IconeButtonType{
        CLOSE,ICONIFY,FULLSCREEN;
    }

    private int buttonImageSize = 30;

    private HashMap<IconeButton,Image> icons = new HashMap<>();

    public enum IconeButton{
        IMAGE_HOVER,IMAGE_DEFAULT,IMAGE_MOUSE_PRESSED,IMAGE_DISABLE;
    }

    public FXButton(IconeButtonType iconeButtonType, String buttonMsg, String tooltips){
        this(buttonMsg,tooltips);
        //Init the Button Icon
        switch (iconeButtonType){
            case CLOSE:
                icons.put(IconeButton.IMAGE_DEFAULT, new Image(FXutils.getRessource("FXButton.icon.default.close")));
                icons.put(IconeButton.IMAGE_HOVER, new Image(FXutils.getRessource("FXButton.icon.hover.close")));
                break;
            case ICONIFY:
                icons.put(IconeButton.IMAGE_DEFAULT, new Image(FXutils.getRessource("FXButton.icon.default.iconify")));
                icons.put(IconeButton.IMAGE_HOVER, new Image(FXutils.getRessource("FXButton.icon.hover.iconify")));
                break;
            case FULLSCREEN:
                icons.put(IconeButton.IMAGE_DEFAULT, new Image(FXutils.getRessource("FXButton.icon.default.fullscreen")));
                icons.put(IconeButton.IMAGE_HOVER, new Image(FXutils.getRessource("FXButton.icon.hover.fullscreen")));
                break;
            default:
                System.err.println("Unrecognized Type  :: "+this.getClass());
                break;
        }
        imageView.setImage(getIcon(IconeButton.IMAGE_DEFAULT));
    }

    public FXButton(Image image, String buttonMsg, String tooltips){
        this(buttonMsg,tooltips);

        icons.put(IconeButton.IMAGE_DEFAULT,image);

        imageView.setImage(getIcon(IconeButton.IMAGE_DEFAULT));
    }

    public FXButton(String path, String buttonMsg, String tooltips){
        this(new Image(path),buttonMsg, tooltips);
    }

    public FXButton(String buttonMsg,String tooltips) {
        super(buttonMsg);
        this.setPrefSize(buttonImageSize,buttonImageSize);


        this.setGraphic(imageView);
        imageView.setFitWidth(buttonImageSize);
        imageView.setPreserveRatio(true);
        imageView.setSmooth(true);

        this.getStyleClass().add("iconButton");

        Tooltip tooltip = new Tooltip(tooltips);

//        Button button = new Button(buttonMsg, imageView);
        this.setTooltip(tooltip);
        this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

        //Add Listeners
        this.setOnMouseEntered(l -> {
            this.setCursor(Cursor.HAND);

            imageView.setImage(getIcon(IconeButton.IMAGE_HOVER));
        });

        this.setOnMouseExited(l -> {
            this.setCursor(Cursor.DEFAULT);

            //Reset image to image 0
            imageView.setImage(getIcon(IconeButton.IMAGE_DEFAULT));
            ArrayList<String> toRemoveClasses = new ArrayList<>();

            //Clear Classes
            for (String styleClass : this.getStyleClass()) {
                if (!styleClass.equalsIgnoreCase("iconButton") && !styleClass.equalsIgnoreCase("button")) {
                    toRemoveClasses.add(styleClass);
                }
            }
            this.getStyleClass().removeAll(toRemoveClasses);

        });

        this.setOnMousePressed(l -> {
            imageView.setImage(getIcon(IconeButton.IMAGE_MOUSE_PRESSED));
            this.getStyleClass().add("iconButtonPressed");
        });

        this.setOnMouseReleased(l -> {
            imageView.setImage(getIcon(IconeButton.IMAGE_DEFAULT));
            //Test if the Button is released or not inside the source
            if (l.getY() > this.getHeight() || l.getY() < 0 ||
                    l.getX() > this.getWidth() || l.getX() < 0) {
                System.out.println("Button Realized outside of the source");
            } else {
                this.getStyleClass().add("iconButtonRealised");
            }
        });

        this.disableProperty().addListener(l -> {
            imageView.setImage(getIcon(IconeButton.IMAGE_DISABLE));
            System.out.println("Button is now disable");
        });
//        button.onActionProperty()
        //Prefer to do action inside MouseReleased
//        button.setOnAction(l -> {
//            System.out.println("Test Action");
//        });
    }

    private Image getIcon(IconeButton  type){
        if ( icons.containsKey(type)  ){
            return icons.get(type);
        }else{
            return getIcon(IconeButton.IMAGE_DEFAULT);
        }
    }

    public void updateIconSize(int newSize){
        buttonImageSize = newSize;
        imageView.setViewport(new Rectangle2D(0,0,newSize,newSize));
    }


    public void setIcon(IconeButton iconType, String path) {
        this.icons.put(iconType, new Image(path));
        if ( iconType == IconeButton.IMAGE_DEFAULT ) {
            imageView.setImage(this.icons.get(IconeButton.IMAGE_DEFAULT));
        }
    }
}
