package sample;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by antoine bergamaschi on 02/04/2015.
 */
public class FXutils {

    private static ResourceBundle resourceBundle;

    public static ResourceBundle getRessourceBundle(){
        if ( resourceBundle == null ){
            resourceBundle = ResourceBundle.getBundle("lang/javafx", Locale.ENGLISH);
        }
        return resourceBundle;
    }


    public static String getRessource(String key){
        return getRessourceBundle().getString(key);
    }

}
