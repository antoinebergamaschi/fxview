package com.soleil.nanoscopium.FXview.utils;

import com.soleil.nanoscopium.rximage.component.FX.utils.lut.RXPixelColor;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;

/**
 * Created by bergamaschi on 10/11/2015.
 */
@XmlRootElement
public class MMXISettings {
    public static MMXISettings instance;

    private String last_openedHdf5 = "";
    private String last_openedSession = "";
    private RXPixelColor.RXColorMap pref_color_map = RXPixelColor.RXColorMap.GrayScale;

    //Constructor
    private MMXISettings(){
        super();
    }

    /**
     * Get an instance of SessionObject with the path to a XML sessionObject file
     */
    public static void open(){
        open(new File(FXutils.getResourceAsString("MMXI.settings.file")));
    }

    /**
     * Get an instance of SessionObject with a XML sessionObject file
     * @param file File, the XML session Object file
     */
    public static void open(File file){
        if ( file.exists() ) {
            try {
                JAXBContext jaxbContext = JAXBContext.newInstance(MMXISettings.class);

                Unmarshaller jaxUnmarshaller = jaxbContext.createUnmarshaller();
//            Schema schema = jaxUnmarshaller.getSchema();
                instance = (MMXISettings) jaxUnmarshaller.unmarshal(file);

            } catch (JAXBException e) {
                e.printStackTrace();
            }
        }
        else{
            instance = new MMXISettings();
        }
    }


    public static void save(){
        instance.writteXML(FXutils.getResourceAsString("MMXI.settings.file"));
    }


    private void writteXML(String fileName){
        File file;
        try{
            file  = new File(fileName);

            JAXBContext jaxbContext = JAXBContext.newInstance(MMXISettings.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(this.instance, file);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public String getLast_openedSession() {
        return last_openedSession;
    }

    public void setLast_openedSession(String last_openedSession) {
        this.last_openedSession = last_openedSession;
    }

    public String getLast_openedHdf5() {
        return last_openedHdf5;
    }

    public void setLast_openedHdf5(String last_openedHdf5) {
        this.last_openedHdf5 = last_openedHdf5;
    }

    public RXPixelColor.RXColorMap getPref_color_map() {
        return pref_color_map;
    }

    public void setPref_color_map(RXPixelColor.RXColorMap pref_color_map) {
        this.pref_color_map = pref_color_map;
    }
}
