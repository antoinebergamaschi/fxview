package com.soleil.nanoscopium.FXview.utils.generic;

import com.soleil.nanoscopium.FXview.utils.FXutils;
import com.soleil.nanoscopium.FXview.utils.MMXISettings;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

/**
 * Created by bergamaschi on 10/11/2015.
 */
public class DialogLoadHdf5 {
    public static void loadHdf5(Stage stage){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(FXutils.getLang("menu.option.open.hdf5.FileChooser.title"));
        fileChooser.setInitialFileName(MMXISettings.instance.getLast_openedHdf5());
        File selected = fileChooser.showOpenDialog(stage.getOwner());
        if ( selected != null ){
            RXTomoJ.getInstance().getModelController().getSessionController().openHdf5(selected.getAbsolutePath());
        }
    }
}
