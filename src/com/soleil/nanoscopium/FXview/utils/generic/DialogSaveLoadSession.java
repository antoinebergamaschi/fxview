package com.soleil.nanoscopium.FXview.utils.generic;

import com.soleil.nanoscopium.FXview.utils.FXutils;
import com.soleil.nanoscopium.FXview.utils.MMXISettings;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.File;
import java.util.Optional;

/**
 * Created by bergamaschi on 10/11/2015.
 */
public class DialogSaveLoadSession {

    public static void saveBeforeClose(Stage stage){
        Dialog<ButtonType> dialog = new Dialog<>();
        ButtonType buttonType = new ButtonType(FXutils.getLang("dialog.close.save"), ButtonBar.ButtonData.YES);
        ButtonType buttonType2 = new ButtonType(FXutils.getLang("dialog.close.continue"), ButtonBar.ButtonData.NO);
        dialog.setTitle(FXutils.getLang("dialog.close.title"));
        dialog.setContentText(FXutils.getLang("dialog.close.message"));
        dialog.initOwner(stage);

        dialog.getDialogPane().getButtonTypes().addAll(buttonType, buttonType2);
        dialog.showAndWait().ifPresent(response -> {
            if ( response == ButtonType.YES ) {
                saveSession(stage);
            }
        });
    }


    public static void saveSession(Stage stage){
        if( RXTomoJ.getInstance().getModelController().getSessionController().canSaveSessionObject() ){
            //Ask if need to erase old file
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle(FXutils.getLang("menu.option.save.alert.title"));
            alert.setHeaderText(FXutils.getLang("menu.option.save.alert.header"));
            alert.setContentText(FXutils.getLang("menu.option.save.alert.content"));
            alert.initOwner(stage);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() != ButtonType.OK){
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle(FXutils.getLang("menu.option.save.FileChooser.title"));
                fileChooser.setInitialFileName("SessionObject.xml");
                File file = fileChooser.showSaveDialog(stage);

                //Test if cancel
                if ( file != null && file.getPath() != null ) {
                    RXTomoJ.getInstance().getModelController().getSessionController().setSessionObjectPath(file.getPath());
                }else{
                    //Do not save
                    return;
                }
            }
            RXTomoJ.getInstance().getModelController().getSessionController().saveSessionObject();
        }
        //Else if cannot save ( do not have a path to save )
        else if ( RXTomoJ.getInstance().getModelController().getSessionController().isSessionOpen() ) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle(FXutils.getLang("menu.option.save.FileChooser.title"));
            fileChooser.setInitialFileName("SessionObject.xml");
            File file = fileChooser.showSaveDialog(stage);

            if ( file != null && !file.exists()) {
                //Update RX-Model
                RXTomoJ.getInstance().getModelController().getSessionController().setSessionObjectPath(file.getPath());
                RXTomoJ.getInstance().getModelController().getSessionController().saveSessionObject();
            }
        }
    }


    public static void newSession(Stage stage){

        if( RXTomoJ.getInstance().getModelController().getSessionController().isSessionOpen() ){
            closeSession(stage);
        }
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(FXutils.getLang("menu.option.save.FileChooser.title"));
        fileChooser.setInitialFileName("SessionObject.xml");
        File file = fileChooser.showSaveDialog(stage);

        if ( file != null && !file.exists()) {
            //Update RX-Model
            RXTomoJ.getInstance().getModelController().getSessionController().setSessionObjectPath(file.getPath());
            RXTomoJ.getInstance().getModelController().getSessionController().saveSessionObject();
        }
    }

    public static void closeSession(Stage stage){
        if( RXTomoJ.getInstance().getModelController().getSessionController().isSessionOpen() ){

            Dialog<ButtonType> dialog = new Dialog<>();
            ButtonType buttonType = new ButtonType(FXutils.getLang("dialog.session.close.button.yes"), ButtonBar.ButtonData.YES);
            ButtonType buttonType2 = new ButtonType(FXutils.getLang("dialog.session.close.button.no"), ButtonBar.ButtonData.NO);
            dialog.setTitle(FXutils.getLang("dialog.session.close.title"));
            dialog.setContentText(FXutils.getLang("dialog.session.close.message"));

            dialog.initOwner(stage);
            dialog.getDialogPane().getButtonTypes().addAll(buttonType, buttonType2);

            dialog.showAndWait().ifPresent(response -> {
                if ( response == ButtonType.YES ) {
                    RXTomoJ.getInstance().getModelController().getSessionController().saveSessionObject();
                }
            });

            RXTomoJ.getInstance().getModelController().getSessionController().closeSessionObject();
        }
    }

    public static void loadSession(Stage stage){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(FXutils.getLang("menu.option.open.session.FileChooser.title"));
        fileChooser.setInitialDirectory(new File(MMXISettings.instance.getLast_openedSession()).getParentFile());
        File file = fileChooser.showOpenDialog(stage);
        if ( file != null ) {
            RXTomoJ.getInstance().getModelController().getSessionController().openSessionObject(file.getPath());
        }
    }
}
