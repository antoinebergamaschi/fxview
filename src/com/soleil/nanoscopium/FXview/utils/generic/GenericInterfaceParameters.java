package com.soleil.nanoscopium.FXview.utils.generic;

import javafx.scene.layout.Pane;

/**
 * Created by bergamaschi on 12/11/2015.
 */
public abstract class GenericInterfaceParameters<T> {
    public T parameterObject;

    public int marginInside = 5;
    public int marginOutside = 10;

    public GenericInterfaceParameters(T parameterObject){
        this.parameterObject = parameterObject;
    }

    public abstract Pane getPane();
    public abstract void update(T newParameterObject);
}
