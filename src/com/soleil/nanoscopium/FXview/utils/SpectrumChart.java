package com.soleil.nanoscopium.FXview.utils;

import com.soleil.nanoscopium.rximage.RXSpectrum;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import javafx.animation.AnimationTimer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.chart.Axis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.effect.BlendMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;

import java.util.ArrayList;


/**
 * Created by bergamaschi on 26/06/2015.
 */
@Deprecated
public class SpectrumChart extends AnchorPane{
    private LineChart<Number,Number> lineChart;
//    private Pane pane;
    private Canvas canvas;
    private double currentLineVal;

    final private Color preankColor = Color.rgb(0,0,0,0.5);
    private double[] lineD = new double[4];
    private double peakWidth = 3;

    private ArrayList<RXSpectrum> spectrums = new ArrayList<>();

    public SpectrumChart(){
        final NumberAxis xAxis = new NumberAxis(1, 2048, 128);
        final NumberAxis yAxis = new NumberAxis();


//        xAxis.setM
        lineChart = new LineChart<>(xAxis,yAxis);
        lineChart.setCreateSymbols(false);

        Label cursorCoords = createCursorGraphCoordsMonitorLabel(lineChart);

//        pane = new AnchorPane();
        canvas = new Canvas();

        AnchorPane.setBottomAnchor(lineChart, 0.0);
        AnchorPane.setTopAnchor(lineChart, 0.0);
        AnchorPane.setRightAnchor(lineChart, 0.0);
        AnchorPane.setLeftAnchor(lineChart, 0.0);

        AnchorPane.setBottomAnchor(canvas, 0.0);
        AnchorPane.setTopAnchor(canvas, 0.0);
        AnchorPane.setRightAnchor(canvas, 0.0);
        AnchorPane.setLeftAnchor(canvas, 0.0);

        this.getChildren().addAll(lineChart, canvas);

        canvas.toFront();
        canvas.setMouseTransparent(true);
        canvas.setBlendMode(BlendMode.SRC_OVER);
        // bind the dimensions when the user resizes the window.
        canvas.widthProperty().bind(this.widthProperty());
        canvas.heightProperty().bind(this.heightProperty());


        lineChart.setOnMouseClicked(mouseEvent -> {
            if ( (double)xAxis.getValueForDisplay(mouseEvent.getX()) > 0.0 ) {
                updateLine(mouseEvent.getX());

                System.out.println("Current Value X :: "+currentLineVal);
            }
        });

        //Create the Timer For Updating the Canvas
        final AnimationTimer timer = new AnimationTimer() {

            @Override public void handle(long now) {
                GraphicsContext gc = canvas.getGraphicsContext2D();
                gc.clearRect(0, 0, SpectrumChart.this.getWidth(), SpectrumChart.this.getHeight());

                updateLine();

                draw(gc);
            }
        };

        timer.start();
    }


    private void draw(GraphicsContext g){
        if ( g != null ){

//            if ( allowPeak ){
                createPeak(g);
//            }
//
//            if ( allowRoi ) {
//                createRecRoi(g);
//            }
//
//            drawCustomShape(g);
        }
    }

    /**
     * Create a Buffered Image on top of the Plot to select a ROI
     * @param g the {@link java.awt.Graphics2D} where to draw the ROI
     */
    private void createPeak(GraphicsContext g){
//        for ( int i = 0 ; i < lineD.length ; i++ ){
//            System.err.println("Line :: "+lineD[i]);
//        }
        g.setFill(preankColor);
        g.fillRect(lineD[0], lineD[1], lineD[2], lineD[3]);
    }

    public void setLog(boolean bol){
        for ( int i = 0 ; i < lineChart.getData().size(); i++ ){
            XYChart.Series<Number, Number> observableFloatArray = lineChart.getData().get(i);
            if ( bol ){
               for ( int k = 0 ; k < observableFloatArray.getData().size(); k++ ){
                   float val = (float) observableFloatArray.getData().get(k).getYValue();
                   if ( val > 0 ){
                       observableFloatArray.getData().get(k).setYValue((float)Math.log(val));
                   }
               }
            }else{
                for ( int k = 0 ; k < observableFloatArray.getData().size(); k++ ){
                    float val = (float) observableFloatArray.getData().get(k).getYValue();
                    if ( val > 0 ){
                        observableFloatArray.getData().get(k).setYValue((float)Math.exp(val));
                    }
                }
            }
        }
    }

    private void updateLine(){
        final NumberAxis yAxis = (NumberAxis) lineChart.getYAxis();
        final NumberAxis xAxis = (NumberAxis) lineChart.getXAxis();

        double posX = xAxis.getDisplayPosition(currentLineVal) + xAxis.getDisplayPosition(xAxis.getLowerBound()) + xAxis.getLayoutX() + 5;

        lineD[0] = posX - peakWidth/2.0d;
        lineD[1] = yAxis.getDisplayPosition(yAxis.getUpperBound()) + yAxis.getLayoutY() + 5;
        lineD[2] = peakWidth;
        lineD[3] = lineD[1] + yAxis.getDisplayPosition(0) - yAxis.getLayoutY() - 5;
    }

    private void updateLine(double posX){
        final NumberAxis yAxis = (NumberAxis) lineChart.getYAxis();
        final NumberAxis xAxis = (NumberAxis) lineChart.getXAxis();

        currentLineVal = (double) xAxis.getValueForDisplay(posX-xAxis.getDisplayPosition(xAxis.getLowerBound()) - xAxis.getLayoutX() - 5);

        currentLineVal = ComputationUtils.findExtremeValue(currentLineVal,spectrums.get(0),10);
        posX = xAxis.getDisplayPosition(currentLineVal) + xAxis.getDisplayPosition(xAxis.getLowerBound()) + xAxis.getLayoutX() + 5;

        lineD[0] = posX - peakWidth/2.0d;
        lineD[1] = yAxis.getDisplayPosition(yAxis.getUpperBound()) + yAxis.getLayoutY() + 5;
        lineD[2] = peakWidth;
        lineD[3] = lineD[1] + yAxis.getDisplayPosition(0) - yAxis.getLayoutY() - 5;
    }

    /**
     *
     * @param spectrum
     * @param title
     * @return
     */
    public String addSerie(RXSpectrum spectrum, String title) {
        spectrums.add(spectrum);
        float[] data = spectrum.copyData();
        float[] x = new float[data.length];
        for ( int i = 0 ; i < data.length ; i++ ){
            x[i] = i;
        }
        XYChart.Series<Number, Number> series = createSeries(x, data);
        series.setName(title);

        lineChart.getData().add(series);

        return "default-color"+(lineChart.getData().size()-1)+".chart-series-line";
    }

    public String addSerie(RXSpectrum spectrum) {
        return addSerie(spectrum,"");
    }

    public void disableSerie(String serieName, boolean dis) {
        for ( int i  = 0 ; i < lineChart.getData().size() ; i++ ){
            if ( lineChart.getData().get(i).getName().equalsIgnoreCase(serieName) ){
                if ( dis ) {
                    lineChart.getData().get(i).nodeProperty().get().getStyleClass().add("chart-series-line-disabled");
                }else {
                    lineChart.getData().get(i).nodeProperty().get().getStyleClass().remove("chart-series-line-disabled");
                }
            }
        }
    }


    private XYChart.Series<Number, Number> createSeries(float[] abs, float[] coor){
        XYChart.Series<Number, Number> series = new XYChart.Series<>();
        ObservableList<XYChart.Data<Number, Number>> observableFloatArray = FXCollections.<XYChart.Data<Number, Number>>observableArrayList();
        if  (abs.length == coor.length){
            for ( int i = 0 ; i < abs.length ; i++ ){
                if ( coor[i] <= 0 ){
                    observableFloatArray.add(new XYChart.Data<>(abs[i], coor[i]));
                }else{
                    observableFloatArray.add(new XYChart.Data<>(abs[i], (float)Math.log(coor[i])));
                }
            }
        }
        series.setData(observableFloatArray);
        return series;
    }

    private Label createCursorGraphCoordsMonitorLabel(LineChart<Number, Number> lineChart) {
        final Axis<Number> xAxis = lineChart.getXAxis();
        final Axis<Number> yAxis = lineChart.getYAxis();

        final Label cursorCoords = new Label();

        final Node chartBackground = lineChart.lookup(".chart-plot-background");
        for (Node n: chartBackground.getParent().getChildrenUnmodifiable()) {
            if (n != chartBackground && n != xAxis && n != yAxis) {
                n.setMouseTransparent(true);
            }
        }


        chartBackground.setOnMouseEntered(mouseEvent -> cursorCoords.setVisible(true));

        chartBackground.setOnMouseMoved(mouseEvent -> cursorCoords.setText(
                String.format(
                        "(%.2f, %.2f)",
                        xAxis.getValueForDisplay(mouseEvent.getX()),
                        yAxis.getValueForDisplay(mouseEvent.getY())
                )
        ));

        chartBackground.setOnMouseExited(mouseEvent -> cursorCoords.setVisible(false));


        xAxis.setOnMouseEntered(mouseEvent -> cursorCoords.setVisible(true));

        xAxis.setOnMouseMoved(mouseEvent -> {
            cursorCoords.setText(
                String.format(
                        "x = %.2f",
                        xAxis.getValueForDisplay(mouseEvent.getX())
            ));
            System.out.println(String.format(
                "x = %.2f",
                xAxis.getValueForDisplay(mouseEvent.getX())
            ));
        });

        xAxis.setOnMouseExited(mouseEvent -> cursorCoords.setVisible(false));

        yAxis.setOnMouseEntered(mouseEvent -> cursorCoords.setVisible(true));

        yAxis.setOnMouseMoved(mouseEvent -> cursorCoords.setText(
                String.format(
                        "y = %.2f",
                        yAxis.getValueForDisplay(mouseEvent.getY())
                )
        ));

        yAxis.setOnMouseExited(mouseEvent -> cursorCoords.setVisible(false));

        return cursorCoords;
    }

}
