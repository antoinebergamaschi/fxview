package com.soleil.nanoscopium.FXview.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by antoine bergamaschi on 02/04/2015.
 */
public class FXutils {

    private static ResourceBundle MMXIBundle;
    private static ResourceBundle resourceBundle;

    private static ResourceBundle getRessourceBundle(){
        if ( resourceBundle == null ){
            resourceBundle = ResourceBundle.getBundle("fxview", Locale.ENGLISH);
        }
        return resourceBundle;
    }

    private static ResourceBundle getLangBundle(){
        if ( MMXIBundle == null ){
            MMXIBundle = ResourceBundle.getBundle("lang/MMXI", Locale.ENGLISH);
        }
        return MMXIBundle;
    }

    public static String getLang(String key){
        return getLangBundle().getString(key);
    }

    public static String getResourceAsString(String key){
        return getRessourceBundle().getString(key);
    }

    public static InputStream getResource(String key){
        try {
            return FXutils.class.getResource(getRessourceBundle().getString(key)).openStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}
