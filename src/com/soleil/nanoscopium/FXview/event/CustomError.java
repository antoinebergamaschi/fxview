package com.soleil.nanoscopium.FXview.event;

/**
 * Created by antoine bergamaschi on 23/04/2015.
 */
public interface CustomError {
    void addErrorListener(ErrorListener errorListener);
    void removeErrorListener(ErrorListener errorListener);
    void removeAllErrorListener();
}
