package com.soleil.nanoscopium.FXview.event;

/**
 * Created by antoine bergamaschi on 22/04/2015.
 */
public interface Notification {
    void addNotificationListener(NotificationListener notificationListener);
    void removeNotificationListener();
}
