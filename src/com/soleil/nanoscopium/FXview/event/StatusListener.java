package com.soleil.nanoscopium.FXview.event;

/**
 * Created by bergamaschi on 15/04/2015.
 */
public interface StatusListener {
    void statusChange(String text, double process);
}
