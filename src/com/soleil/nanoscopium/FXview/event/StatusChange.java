package com.soleil.nanoscopium.FXview.event;

/**
 * Created by bergamaschi on 15/04/2015.
 */
public interface StatusChange {
    void addStatusListener(StatusListener statusListener);
    void removeStatusListener();
}
