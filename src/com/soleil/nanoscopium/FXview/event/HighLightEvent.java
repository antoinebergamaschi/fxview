package com.soleil.nanoscopium.FXview.event;

/**
 * Created by bergamaschi on 15/04/2015.
 */
public interface HighLightEvent {
    void addHighLightListener(HighLightListener highLightListener);
    void removeHighLightListener();
}
