package com.soleil.nanoscopium.FXview.event;

/**
 * Created by antoine bergamaschi on 23/04/2015.
 */
public interface ErrorListener {
    void error(String text);
}
