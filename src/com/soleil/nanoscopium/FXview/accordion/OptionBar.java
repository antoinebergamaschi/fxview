package com.soleil.nanoscopium.FXview.accordion;

import com.soleil.nanoscopium.FXview.accordion.option.AccordionOptionalPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

import java.util.ArrayList;

/**
 * Created by bergamaschi on 07/08/2015.
 */
public abstract class OptionBar extends GridPane {

    protected AccordionPane accordionPane;
    protected Pane container;
    protected ArrayList<AccordionOptionalPane> accordionOptionalPanes = new ArrayList<>();

    protected double containerSize = 200;

    public enum OptionBarOrientation{
        HORIZONTAL,VERTICAL
    }

    protected OptionBar(AccordionPane accordionPane, Pane container){
        this.container = container;
        this.accordionPane = accordionPane;
        this.getStyleClass().add("accordion-option-bar");
    }

    final private ToggleGroup group = new ToggleGroup();

    protected void addToggleButton(ToggleButton toggleButton){
        toggleButton.setToggleGroup(group);
    }

    public void updateContainerSize(double newSize){
        containerSize = newSize;
    }

    /**
     * Add To the OptionBar a Option Toggle Button associated with the
     * Container.
     * @param accordionOptionalPane
     */
    abstract void addToBar(AccordionOptionalPane accordionOptionalPane);
}
