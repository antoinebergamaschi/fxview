package com.soleil.nanoscopium.FXview.accordion;

import com.soleil.nanoscopium.FXview.accordion.option.AccordionOptionalPane;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;


/**
 * Created by antoine bergamaschi on 23/07/2015.
 */
public class AccordionPane extends GridPane {
    private final static double rem = javafx.scene.text.Font.getDefault().getSize();
    public final static double optionBarSize = 2*rem;
    private static boolean lockMovementDetection = false;

    private AccordionPane self;

    //Size of the Panels
    private final DoubleProperty leftPanelSize = new SimpleDoubleProperty(200);
    private final DoubleProperty bottomPanelSize = new SimpleDoubleProperty(200);
    //Double use to save the location of the precedent pointer position when dragged
    private double positionXResizeLeftPanelD = -1;
    private double positionXResizeBottomPanelD = -1;

    private double minLeftPanelSize = 20;
    private double minBottomPanelSize = 20;

    //Boolean use to reset the cursor to default when needed
    private boolean isInPanel = false;

    private AnchorPane centerContent;
    private AnchorPane leftContentNode;
    private AnchorPane bottomContentNode;

    private OptionBarV barLeft;
    private OptionBarH barBottom;

    public double panelSize(OptionBar.OptionBarOrientation orientation){
        double size = 0;
        switch (orientation){
            case HORIZONTAL:
                break;
            case VERTICAL:
                break;
        }
        return size;
    }

    public AccordionPane(){
        self = this;

        centerContent = new AnchorPane();

        leftContentNode = new AnchorPane();
        leftContentNode.getStyleClass().add("leftPanelAccordionPanBar");

        bottomContentNode = new AnchorPane();
        bottomContentNode.getStyleClass().add("bottomPanelAccordionPanBar");

        initEventLeftPane();
        initEventBottomPane();

        //set event to trigger either the bottom or the left resize
        this.addEventHandler(MouseEvent.MOUSE_MOVED,l->{
            if (!lockMovementDetection) {
                isInPanel = false;
                if (self.getChildren().contains(leftContentNode)) {
                    //Fire the resize Event for the leftComponent
                    if (isInNodeLeft(leftContentNode, l.getX(), l.getY(), 1 * rem)) {
                        this.getScene().setCursor(Cursor.E_RESIZE);
                        isInPanel = true;
                    }
                }

                if (self.getChildren().contains(bottomContentNode)) {
                    //Fire the resize Event for the bottomComponent
                    if (isInNodeBottom(bottomContentNode, l.getX(), l.getY(), 1 * rem)) {
                        this.getScene().setCursor(Cursor.N_RESIZE);
                        isInPanel = true;
                    }
                }

                if (!isInPanel) {
                    this.getScene().setCursor(Cursor.DEFAULT);
                }
                l.consume();
            }
        });

        //Init the optionBar this option are not visible unless Option is added
        barLeft = new OptionBarV(this,leftContentNode);
        barBottom = new OptionBarH(this,bottomContentNode);

        leftPanelSize.addListener(l -> {
            barLeft.updateContainerSize(leftPanelSize.doubleValue());
            leftContentNode.setMinWidth(leftPanelSize.doubleValue());
            leftContentNode.setPrefWidth(leftPanelSize.doubleValue());
        });


        leftContentNode.setPrefWidth(0);
        bottomContentNode.setPrefWidth(0);

        bottomPanelSize.addListener(l -> {
            barBottom.updateContainerSize(bottomPanelSize.doubleValue());
            bottomContentNode.setMinHeight(bottomPanelSize.doubleValue());
            bottomContentNode.setPrefHeight(bottomPanelSize.doubleValue());
        });


        self.widthProperty().addListener(l->{
            minLeftPanelSize = 0.1*self.getWidth();
        });

        self.heightProperty().addListener(l->{
            minBottomPanelSize = 0.1*self.getHeight();
        });


//        GridPane.setConstraints(barLeft, 2, 0, 1, 3, HPos.RIGHT, VPos.TOP, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));
//        GridPane.setConstraints(leftContentNode, 1, 0, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));

        GridPane.setConstraints(centerContent, 0, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));

//        GridPane.setConstraints(barBottom, 0, 2, 3, 1, HPos.LEFT, VPos.BOTTOM, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));
//        GridPane.setConstraints(bottomContentNode, 0, 1, 2, 1, HPos.CENTER, VPos.TOP, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));


        this.getChildren().addAll(centerContent);
    }


    public void setCenterContent(Pane node){
        //Remove the old node if any
        if ( centerContent.getChildren().size() > 0 ){
            centerContent.getChildren().clear();
        }

        AnchorPane.setBottomAnchor(node, 0.0d);
        AnchorPane.setTopAnchor(node, 0.0d);
        AnchorPane.setRightAnchor(node, 0.0d);
        AnchorPane.setLeftAnchor(node, 0.0d);

        //Add the new contentNNode to the current accordion
        centerContent.getChildren().add(node);
        centerContent.setMinWidth(0);
        centerContent.setMinHeight(0);
    }

    public void addOption(OptionBar.OptionBarOrientation orientation, AccordionOptionalPane accordionOptionalPane){
        switch (orientation){
            case HORIZONTAL:
                barBottom.addToBar(accordionOptionalPane);
                //Add it if not already
                if ( !this.getChildren().contains(barBottom) ){
                    this.getChildren().add(barBottom);
                    reconfOptionBar();
                }
                break;
            case VERTICAL:
                barLeft.addToBar(accordionOptionalPane);
                //Add it if not already
                if ( !this.getChildren().contains(barLeft) ){
                    this.getChildren().add(barLeft);
                    reconfOptionBar();
                }
                break;
        }
    }

    public void reconfOptionBar(){
        if ( self.getChildren().contains(barLeft) && self.getChildren().contains(barLeft) ) {
            if (!self.getChildren().contains(leftContentNode)) {
                GridPane.setConstraints(bottomContentNode, 0, 1, 1, 1, HPos.CENTER, VPos.TOP, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));
            } else {
                //Work around set 3 here
                GridPane.setConstraints(bottomContentNode, 0, 1, 2, 1, HPos.CENTER, VPos.TOP, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));
            }
            if (!self.getChildren().contains(bottomContentNode)) {
                GridPane.setConstraints(leftContentNode, 1, 0, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));
            }else{
                GridPane.setConstraints(leftContentNode, 1, 0, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));
            }

            GridPane.setConstraints(barLeft, 2, 0, 1, 3, HPos.RIGHT, VPos.TOP, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));
            GridPane.setConstraints(barBottom, 0, 2, 3, 1, HPos.LEFT, VPos.BOTTOM, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));

        }
        else if ( self.getChildren().contains(barLeft) ){
            GridPane.setConstraints(leftContentNode, 1, 0, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));
            GridPane.setConstraints(barLeft, 2, 0, 1, 1, HPos.RIGHT, VPos.TOP, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));
        }
        else if ( self.getChildren().contains(barBottom) ) {
            GridPane.setConstraints(bottomContentNode, 0, 1, 1, 1, HPos.CENTER, VPos.TOP, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));
            GridPane.setConstraints(barBottom, 0, 2, 1, 1, HPos.LEFT, VPos.BOTTOM, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));
        }

        self.requestLayout();
    }

    private boolean isInNodeLeft(Region node, double positionX, double positionY, double overShoot){
        if (positionX < node.getLayoutX() + overShoot && positionX > node.getLayoutX() - overShoot  &&
                positionY > node.getLayoutY() && positionY < node.getLayoutY() + node.getHeight()) {
            return true;
        }
        return false;
    }

    private boolean isInNodeBottom(Region node, double positionX, double positionY, double overShoot){
        if (positionX > node.getLayoutX() && positionX < node.getLayoutX() + node.getWidth() &&
                positionY < node.getLayoutY() + overShoot && positionY > node.getLayoutY() - overShoot) {
            return true;
        }
        return false;
    }

    private void initEventLeftPane(){
        //Event For resizing LeftContentNode
        this.addEventHandler(MouseEvent.MOUSE_DRAGGED, l -> {
            if (this.getScene().getCursor() == Cursor.E_RESIZE) {

                if ( self.getWidth() - l.getX() > minLeftPanelSize && l.getX() >= minLeftPanelSize) {
                    leftPanelSize.set(leftPanelSize.getValue() + positionXResizeLeftPanelD - l.getX());
                }

                positionXResizeLeftPanelD = l.getX();

                l.consume();
            }
        });

        this.addEventHandler(MouseEvent.MOUSE_PRESSED, l->{
            if (this.getScene().getCursor() == Cursor.E_RESIZE) {
                positionXResizeLeftPanelD = l.getX();
                lockMovementDetection = true;
                l.consume();
            }
        });

        this.addEventHandler(MouseEvent.MOUSE_RELEASED, l->{
            if (this.getScene().getCursor() == Cursor.E_RESIZE ) {
                if (leftPanelSize.doubleValue() < minLeftPanelSize) {
                    leftPanelSize.set(minLeftPanelSize);
                }
//                leftContentNode.setMinWidth(leftPanelSize);
                lockMovementDetection = false ;
                this.getScene().setCursor(Cursor.DEFAULT);
                l.consume();
            }
        });
    }

    private void initEventBottomPane() {
        //Event For resizing LeftContentNode
        this.addEventHandler(MouseEvent.MOUSE_DRAGGED, l -> {
            if (this.getScene().getCursor() == Cursor.N_RESIZE ) {
                if ( self.getHeight() - l.getY() > minBottomPanelSize && l.getY() >= minBottomPanelSize) {
                    bottomPanelSize.set(bottomPanelSize.getValue() + positionXResizeBottomPanelD - l.getY());
                }
                positionXResizeBottomPanelD = l.getY();

                l.consume();
            }
        });

        this.addEventHandler(MouseEvent.MOUSE_PRESSED, l->{
            if (this.getScene().getCursor() == Cursor.N_RESIZE ) {
                positionXResizeBottomPanelD = l.getY();
                lockMovementDetection = true;
                l.consume();
            }
        });

        this.addEventHandler(MouseEvent.MOUSE_RELEASED, l->{
            if (this.getScene().getCursor() == Cursor.N_RESIZE ) {
                if (bottomPanelSize.doubleValue() < minBottomPanelSize) {
                    bottomPanelSize.set(minBottomPanelSize);
                }
//                bottomContentNode.setMinHeight(bottomPanelSize);
                this.getScene().setCursor(Cursor.DEFAULT);
                lockMovementDetection = false;
                l.consume();
            }
        });
    }
}

