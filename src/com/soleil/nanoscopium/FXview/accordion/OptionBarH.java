package com.soleil.nanoscopium.FXview.accordion;

import com.soleil.nanoscopium.FXview.accordion.option.AccordionOptionalPane;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.*;

/**
 * Created by bergamaschi on 07/08/2015.
 */
public class OptionBarH extends OptionBar {


    public OptionBarH(AccordionPane accordionPane, Pane container) {
        super(accordionPane, container);
        this.getStyleClass().add("H");
        this.setPrefHeight(AccordionPane.optionBarSize);
        this.setMinHeight(AccordionPane.optionBarSize);
        this.setMaxHeight(AccordionPane.optionBarSize);
    }

    @Override
    public void addToBar(AccordionOptionalPane accordionOptionalPane) {
        accordionOptionalPanes.add(accordionOptionalPane);
        ToggleButton icon = accordionOptionalPane.getIcon();
        String title = accordionOptionalPane.getTitle();
        icon.setText(title);
        addToggleButton(icon);

        icon.getStyleClass().add("accordion-option-bar-title-H");

        GridPane.setConstraints(icon, this.getChildren().size(), 0 , 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.ALWAYS, new Insets(0, 0, 0, 0));

        this.getChildren().add(icon);

        //Open or close the LeftPanel with the panel
        icon.setOnMouseClicked(l -> {
            if (icon.isSelected()) {
                boolean doTransformation = accordionPane.getChildren().contains(container);

                ScrollPane scrollPane = accordionOptionalPane.getPane();

                scrollPane.setFitToWidth(true);

                AnchorPane.setBottomAnchor(scrollPane, 0.0d);
                AnchorPane.setTopAnchor(scrollPane, 0.0d);
                AnchorPane.setRightAnchor(scrollPane, 0.0d);
                AnchorPane.setLeftAnchor(scrollPane, 0.0d);

                container.heightProperty().addListener(w -> {
                    //TODO -4?? border layout ?
                    //Test Snap
                    ((Region) scrollPane.getContent()).setMinHeight(container.getHeight() - 4);
                });

                if (doTransformation) {
                    container.getChildren().clear();
                }else{
                    accordionPane.getChildren().add(container);
                }

                container.getChildren().add(scrollPane);

                if ( !doTransformation ) {
                    AccordionTransition accordionTransition = new AccordionTransition(container, containerSize, AccordionTransition.AnimationDirection.NORTH);
                    accordionTransition.play();
                }else{
                    container.setPrefHeight(containerSize);
                }
                accordionPane.reconfOptionBar();
            } else {
                AccordionTransition accordionTransition = new AccordionTransition(container, containerSize, AccordionTransition.AnimationDirection.SOUTH);
                accordionTransition.play();
                accordionTransition.setOnFinished(k -> {
                    container.getChildren().clear();
                    //Remove the pane to completely remove visually the pane
                    accordionPane.getChildren().remove(container);
                    accordionPane.reconfOptionBar();
                });
            }
        });
    }
}
