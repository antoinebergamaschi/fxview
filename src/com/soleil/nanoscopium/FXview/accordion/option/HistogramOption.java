package com.soleil.nanoscopium.FXview.accordion.option;

import com.soleil.nanoscopium.FXview.utils.FXutils;
import com.soleil.nanoscopium.FXview.utils.MMXISettings;
import com.soleil.nanoscopium.rximage.component.FX.RXImageCanvas;
import com.soleil.nanoscopium.rximage.component.FX.utils.lut.RXPixelColor;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;

import java.util.ArrayList;

/**
 * Created by bergamaschi on 23/10/2015.
 */
public class HistogramOption extends AccordionOptionalPane<RXImageCanvas> {

    private Slider minValue;
    private Slider maxValue;
    private LineChart lineChart;
    private GridPane additionalData = new GridPane();


    public HistogramOption(RXImageCanvas view) {
        super(view);
    }

    @Override
    protected ToggleButton getDefaultIcon() {
        ToggleButton arrowLeft = new ToggleButton("",arrowIcon);

        arrowIcon.setIcon(FontAwesomeIcon.COG);

        return arrowLeft;
    }

    @Override
    public String getTitle() {
        return FXutils.getLang("accordion.histo.title");
    }

    @Override
    protected Pane getDefaultPane() {
        view.addRXImageListener(()->{
            update();
        });

        final GridPane pane = new GridPane();


        final CheckBox auto = new CheckBox(FXutils.getLang("accordion.histo.check.auto"));
        auto.setSelected(view.isAutoColorUpdate());
        auto.setOnAction(l->{
            view.setAutoColorUpdate(auto.isSelected());
        });
        final ComboBox<RXPixelColor.RXColorMap> colorMap = new ComboBox<>();
        colorMap.getItems().addAll(RXPixelColor.RXColorMap.getDefaultValues());


        final RXPixelColor pixelColor = view.getPixelColor();

        createCustomColorMapOption(pixelColor);

        //Set Selected the Default MMMXI settings color map
        colorMap.getSelectionModel().select(pixelColor.getColorMap());

        colorMap.valueProperty().addListener((observable, oldValue, newValue) -> {
            //Update Default Settings value
            MMXISettings.instance.setPref_color_map(newValue);

            //Update Pixel Color
            pixelColor.setColorMap(newValue);
            //Update interface
            createCustomColorMapOption(pixelColor);
            //Ask image to be updated
            view.redraw();
        });

        Label minLabel = new Label(FXutils.getLang("accordion.histo.label.min"));
        Label maxLabel = new Label(FXutils.getLang("accordion.histo.label.max"));

        float[] range = pixelColor.getHistoRange();
        maxValue = new Slider(range[0],range[1],0);
        minValue = new Slider(range[0],range[1],0);

        maxValue.setBlockIncrement(1);
        minValue.setBlockIncrement(1);


        final Label minLabelValue = new Label("0");
        final Label maxLabelValue = new Label("0");

        minValue.valueProperty().addListener((observable1, oldValue1, newValue1) -> {
            maxValue.setMin(newValue1.doubleValue());
            minLabelValue.setText(newValue1.intValue()+"");
            view.updateIntensity((float)minValue.getValue(),(float)maxValue.getValue());
            updateChart(pixelColor);
        });

        maxValue.valueProperty().addListener(((observable, oldValue, newValue) -> {
            minValue.setMax(newValue.doubleValue());
            maxLabelValue.setText(newValue.intValue()+ "");
            view.updateIntensity((float)minValue.getValue(),(float)maxValue.getValue());
            updateChart(pixelColor);
        }));


        //Add the Change to Onclik use LogScale
        final CheckBox logScale = new CheckBox(FXutils.getLang("accordion.histo.useLog"));
        //Update the FilterWhen checkBox is clicked
        //TODO bug with the LogScale histo is not recomputed
        logScale.setOnAction(l->{
            view.setUseLogScale(logScale.isSelected());
        });

        GridPane.setConstraints(colorMap, 0, 0, 4, 1, HPos.LEFT, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(5, 0, 0, 5));
        GridPane.setConstraints(auto, 1, 0, 1, 1, HPos.RIGHT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(5, 5, 0, 0));
        GridPane.setConstraints(logScale, 2, 0, 1, 1, HPos.RIGHT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(5, 5, 0, 0));


        GridPane.setConstraints(minLabel, 0, 1, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(5, 0, 5, 5));
        GridPane.setConstraints(minValue, 1, 1, 1, 1, HPos.LEFT, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(5, 0, 0, 0));
        GridPane.setConstraints(minLabelValue, 2, 1, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(5, 10, 0, 0));


        GridPane.setConstraints(maxLabel, 3, 1, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(5, 5, 0, 10));
        GridPane.setConstraints(maxValue, 4, 1, 1, 1, HPos.LEFT, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(5, 5, 0, 0));
        GridPane.setConstraints(maxLabelValue, 5, 1, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(5, 15, 0, 0));

        GridPane.setConstraints(additionalData, 0, 2, 6, 1, HPos.LEFT, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 5, 0));


        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setForceZeroInRange(false);

        lineChart = new LineChart(xAxis,yAxis);
        lineChart.setLegendVisible(false);
        lineChart.setCreateSymbols(false);

        update();

        GridPane.setConstraints(lineChart, 0, 3, 6, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));

        pane.getChildren().addAll(minLabel,auto,logScale, maxLabel, minValue, maxValue, lineChart, additionalData,colorMap,
                maxLabelValue,minLabelValue);

        return pane;
    }

    public void update(){
        RXPixelColor pixelColor = view.getPixelColor();

        //Update the range of the slider
        float[] range = pixelColor.getHistoRange();
        maxValue.setMin(range[0]);
        maxValue.setMax(range[1]);

        minValue.setMin(range[0]);
        minValue.setMax(range[1]);

        for ( ReadOnlyDoubleProperty l : pixelColor.getColorParameters()){
            if ( l.getName().equalsIgnoreCase("min")){
                minValue.setValue(l.doubleValue());
                maxValue.setMin(l.doubleValue());
            }else if ( l.getName().equalsIgnoreCase("max")){
                maxValue.setValue(l.doubleValue());
                minValue.setMax(l.doubleValue());
            }
        }


        updateChart(pixelColor);
    }

    private void updateChart(RXPixelColor pixelColor){
        //Compute the Range of the lineChart ( if Min/Max have been modifyed )
        float[] minMax = pixelColor.getHistoRange();
        int start = (int) (minValue.getValue()/((minMax[1]-minMax[0])/255.0d));
        int end = (int) (maxValue.getValue()/((minMax[1]-minMax[0])/255.0d));


        int[] coor = pixelColor.getCount();
        int[] abs = new int[coor.length];

        for ( int i = start ; i < end ; i++ ){
            abs[i] = i;
        }

        XYChart.Series<Number, Number> series = new XYChart.Series<>();
        ObservableList<XYChart.Data<Number, Number>> observableFloatArray = FXCollections.<XYChart.Data<Number, Number>>observableArrayList();
        for ( int i = start ; i < end ; i++ ){
            observableFloatArray.add(new XYChart.Data<>(abs[i], coor[i]));
        }
        series.setData(observableFloatArray);
        lineChart.getData().clear();
        lineChart.getData().add(series);
    }

    private void createCustomColorMapOption(RXPixelColor color){
        additionalData.getChildren().clear();

        ArrayList<DoubleProperty> parameters = color.getColorMap().getParameters();
//        ArrayList<DoubleProperty> param = color.getColorParameters();

        if ( parameters != null && parameters.size() > 0 ){
            int i = 0;
//            for ( int i = 0 ; i < parameters.length ; i++ ){
            for ( DoubleProperty parameter : parameters ) {
//                    if ( parameter.getName().equalsIgnoreCase(parameters[i]) ){
                Label label = new Label(parameter.getName());
                Slider slider = new Slider(0,1,parameter.getValue());
                slider.valueProperty().addListener((observable,oldValue,newValue)->{
                        parameter.setValue(newValue);
                        view.redraw();
                    }
                );

                GridPane.setConstraints(label, 0, i, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));
                GridPane.setConstraints(slider, 1, i, 1, 1, HPos.LEFT, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 0));

                additionalData.getChildren().addAll(label,slider);
                i++;
            }

        }
    }
}
