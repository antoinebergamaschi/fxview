package com.soleil.nanoscopium.FXview.accordion.option;

import com.soleil.nanoscopium.FXview.accordion.AccordionPane;
import com.soleil.nanoscopium.FXview.accordion.OptionBar;
import com.soleil.nanoscopium.FXview.content.FluoContentPane;
import com.soleil.nanoscopium.FXview.utils.FXutils;
import com.soleil.nanoscopium.nistloader.Elements;
import com.soleil.nanoscopium.nistloader.NistData;
import com.soleil.nanoscopium.nistloader.TransitionType;
import com.soleil.nanoscopium.nistloader.view.ElementTable;
import com.soleil.nanoscopium.nistloader.view.ElementTableListener;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;

/**
 * Created by bergamaschi on 04/08/2015.
 */
public class MendeleevOption extends AccordionOptionalPane<FluoContentPane> implements ElementTableListener{
    public MendeleevOption(FluoContentPane view) {
        super(view);
    }

    private ElementInformation infoPane;
    private ListView<String> listTransition;
    private GridPane hoverInfo;

    @Override
    protected ToggleButton getDefaultIcon() {
        ToggleButton arrowLeft = new ToggleButton("",arrowIcon);
        arrowIcon.setIcon(FontAwesomeIcon.TH);
        return arrowLeft;
    }

    @Override
    public String getTitle() {
        return "Test MF";
    }

    @Override
    protected Pane getDefaultPane() {

        AccordionPane accordionPane = new AccordionPane();

        hoverInfo = new GridPane();
        hoverInfo.getStyleClass().add("table");
        infoPane = new ElementInformation(view);

        ElementTable elementTable = new ElementTable();
        elementTable.addElementTableEvent(infoPane);
        elementTable.addElementTableEvent(this);

        GridPane gridPane = new GridPane();
        GridPane.setConstraints(elementTable, 0, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));
        GridPane.setConstraints(hoverInfo, 1, 0, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.ALWAYS, new Insets(0, 0, 0, 0));

        gridPane.getChildren().addAll(elementTable,hoverInfo);

        accordionPane.setCenterContent(gridPane);
        accordionPane.addOption(OptionBar.OptionBarOrientation.VERTICAL, infoPane);

        return accordionPane;
    }

    @Override
    public void selected(boolean b, Elements elements) {
        int i = 0;
        int j = 0;
        hoverInfo.getChildren().clear();

        Label elementName = new Label(elements.getFullName());


        if ( NistData.getData(elements) != null ) {
            int size = Math.round(NistData.getData(elements).getFluorescenceLine().size() / 4) * 2;

            GridPane.setConstraints(elementName, j, i, size, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 0));
            i++;
            for (TransitionType transitionType : NistData.getData(elements).getFluorescenceLine().keySet()) {
                Label transitionName = new Label(transitionType.name());
                Label transitionValue = new Label(NistData.getData(elements).getFluorescenceLine().get(transitionType)[0] + "");
                GridPane.setConstraints(transitionName, j, i, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));
                GridPane.setConstraints(transitionValue, j+1, i, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));
                i++;
                if ( i > 6 ) {
                    i = 1;
                    j+=2;
                }
                transitionName.getStyleClass().add("tableElement");
                transitionValue.getStyleClass().add("tableElement");

                hoverInfo.getChildren().addAll(transitionName, transitionValue);
            }
        }else{
            GridPane.setConstraints(elementName, j, i,  1 , 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 0));
            i++;

            Label errorNoInfo = new Label(FXutils.getLang("accordionPane.MendeleevOption.errorNoData"));
            GridPane.setConstraints(errorNoInfo, 0, i, 2, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 0));
            hoverInfo.getChildren().add(errorNoInfo);
        }

        hoverInfo.getChildren().add(elementName);
    }

    @Override
    public void hover(Elements elements) {
//        int i = 0;
//        int j = 0;
//        hoverInfo.getChildren().clear();
//
//        Label elementName = new Label(elements.getFullName());
//
//
//        if ( NistData.getData(elements) != null ) {
//            int size = NistData.getData(elements).getFluorescenceLine().size()/4 + 1;
//
//            GridPane.setConstraints(elementName, j, i,  size , 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 0));
//            i++;
//            for (TransitionType transitionType : NistData.getData(elements).getFluorescenceLine().keySet()) {
//                Label transitionName = new Label(transitionType.name());
//                Label transitionValue = new Label(NistData.getData(elements).getFluorescenceLine().get(transitionType)[0] + "");
//                GridPane.setConstraints(transitionName, j, i, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 0));
//                GridPane.setConstraints(transitionValue, j+1, i, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 0));
//                i++;
//                if ( i > 6 ) {
//                    i = 1;
//                    j+=2;
//                }
//
//                transitionName.getStyleClass().add("tableElement");
//                transitionValue.getStyleClass().add("tableElement");
//
//                hoverInfo.getChildren().addAll(transitionName, transitionValue);
//            }
//        }else{
//            GridPane.setConstraints(elementName, j, i,  1 , 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 0));
//            i++;
//
//            Label errorNoInfo = new Label(FXutils.getLang("accordionPane.MendeleevOption.errorNoData"));
//            GridPane.setConstraints(errorNoInfo, 0, i, 2, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 0));
//            hoverInfo.getChildren().add(errorNoInfo);
//        }
//
//        hoverInfo.getChildren().add(elementName);
    }
}
