package com.soleil.nanoscopium.FXview.accordion.option;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;

/**
 * Created by bergamaschi on 03/08/2015.
 *
 * Class used to Display additional data for an {@see AccordionPane}.
 * This class is always link to a {@link <T>view}. This link allow to
 * update and interact with the current view.
 */
public abstract class AccordionOptionalPane<T>{

    final double rem = javafx.scene.text.Font.getDefault().getSize();

    protected T view;

    //TODO CHANGE to Toggle Button from GlyphsDude.createIcon(...)
    protected FontAwesomeIconView arrowIcon = new FontAwesomeIconView();

    protected AccordionOptionalPane(T view){
        this.view = view;
    }

    protected abstract ToggleButton getDefaultIcon();

    public ToggleButton getIcon(){
        ToggleButton toggleButton = getDefaultIcon();
        arrowIcon.setStyle("-glyph-size : "+(1.75*rem)+"px;");
        arrowIcon.getStyleClass().add("accordion-option-icon");
        toggleButton.getStyleClass().add("accordion-option-toggleButton");
        return toggleButton;
    }

    public abstract String getTitle();

    /**
     * Construct the principal AccordionOptionalPane.
     * Cosmetic transformation is performed internally by AccordionOptionalPanel class
     * @return Pane The Basic default pane
     */
    protected abstract Pane getDefaultPane();


    /**
     * Retrieve this AccordionOptionalPane default Pane with the decoration associated
     * and embed in a ScrollPane.
     * @return ScrollPane the scrollPane containing the default Pane
     */
    public ScrollPane getPane(){
        Pane pane = getDefaultPane();
        AnchorPane anchorPane = new AnchorPane();

        AnchorPane.setBottomAnchor(pane, 0.0d);
        AnchorPane.setLeftAnchor(pane, 0.0d);
        AnchorPane.setRightAnchor(pane, 0.0d);
        AnchorPane.setTopAnchor(pane, 0.0d);

        anchorPane.getChildren().add(pane);
        anchorPane.setPrefSize(Region.USE_PREF_SIZE,Region.USE_PREF_SIZE);

        anchorPane.getStyleClass().add("accordion-option-pane");

        ScrollPane scrollPane = new ScrollPane(anchorPane);

        return scrollPane;
    }
}
