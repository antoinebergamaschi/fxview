package com.soleil.nanoscopium.FXview.accordion.option;

import com.soleil.nanoscopium.FXview.module.SpotDetectionModule;
import com.soleil.nanoscopium.FXview.utils.FXutils;
import com.soleil.nanoscopium.rximage.component.RXImageComponentFX;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.util.converter.IntegerStringConverter;

/**
 * Created by bergamaschi on 23/10/2015.
 */
public class SpotRoiOption extends AccordionOptionalPane<RXImageComponentFX> {

    final private TextField xStartPosition = new TextField();
    final private TextField yStartPosition = new TextField();
    final private TextField width = new TextField();
    final private TextField height = new TextField();

    final private double margin_left_right = 5;
    final private double margin_top_bottom = 10;

    public SpotRoiOption(RXImageComponentFX view) {
        super(view);
    }

    @Override
    protected ToggleButton getDefaultIcon() {
        ToggleButton arrowLeft = new ToggleButton("",arrowIcon);

        arrowIcon.setIcon(FontAwesomeIcon.COG);

        return arrowLeft;
    }

    @Override
    public String getTitle() {
        return FXutils.getLang("accordion.option.spotRoi.title");
    }

    @Override
    protected Pane getDefaultPane() {

        //Listen for model update
        RXTomoJ.getInstance().getModelController().addMMXIControllerUpdatedListener(l->{
            if ( l.getEventID() == MMXIControllerEvent.MMXIControllerEventID.REDUCTIONINFO_TRANSMISSION_ROI ){
                if ( l.getEventType() == MMXIControllerEvent.MMXIControllerEventType.UPDATE ){
                    int[][] t = RXTomoJ.getInstance().getModelController().getDataController().getReductionInfo().getTransmissionROI();
                    update(t);
                }
            }
        });

        GridPane pane = new GridPane();


        //Add a TitlePane to change and reset computation of the spot
        SpotDetectionModule spotDetectionModule = new SpotDetectionModule(RXTomoJ.getInstance().getModelController().getDataController().getReductionInfo());
        TitledPane titledPaneSpot = new TitledPane(FXutils.getLang("accordion.option.spotRoi.titlePane.title.spot"),spotDetectionModule.getPane());

//        titledPaneSpot.setCollapsible(false);
        titledPaneSpot.setExpanded(false);
//        TitledPane titledPaneHotSpot = new TitledPane(FXutils.getLang("accordion.option.spotRoi.titlePane.title.hot"),null);


        Label roiLabel = new Label(FXutils.getLang("accordion.option.spotRoi.label.roi"));
        TextFormatter<Integer> t = new TextFormatter<>(new IntegerStringConverter(),0);
        TextFormatter<Integer> t1 = new TextFormatter<>(new IntegerStringConverter(),0);
        TextFormatter<Integer> t2 = new TextFormatter<>(new IntegerStringConverter(),0);
        TextFormatter<Integer> t3 = new TextFormatter<>(new IntegerStringConverter(),0);

//            TextField xPosition = new TextField();
        xStartPosition.setTextFormatter(t);
        Label xPositionLabel = new Label(FXutils.getLang("accordion.option.spotRoi.label.x"));
        xPositionLabel.setLabelFor(xStartPosition);

//            TextField yPosition = new TextField();
        yStartPosition.setTextFormatter(t1);
        Label yPositionLabel = new Label(FXutils.getLang("accordion.option.spotRoi.label.y"));
        yPositionLabel.setLabelFor(yStartPosition);

//            TextField thetaPosition = new TextField();
        width.setTextFormatter(t2);
        Label widthLabel = new Label(FXutils.getLang("accordion.option.spotRoi.label.width"));
        widthLabel.setLabelFor(width);

        height.setTextFormatter(t3);
        Label heightLabel = new Label(FXutils.getLang("accordion.option.spotRoi.label.height"));
        heightLabel.setLabelFor(height);

        GridPane.setConstraints(titledPaneSpot, 0, 0, 5, 1, HPos.CENTER, VPos.TOP, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));

        GridPane.setConstraints(roiLabel, 0, 1, 4, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.ALWAYS, new Insets(0, 0, 0, margin_left_right + 5));
        GridPane.setConstraints(xPositionLabel, 1, 2, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.ALWAYS, new Insets(margin_top_bottom, 0, margin_top_bottom, margin_left_right));
        GridPane.setConstraints(xStartPosition, 2, 2, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(margin_top_bottom, margin_left_right, margin_top_bottom, 0));
        GridPane.setConstraints(yPositionLabel, 3, 2, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.ALWAYS, new Insets(margin_top_bottom, 0, margin_top_bottom, margin_left_right));
        GridPane.setConstraints(yStartPosition, 4, 2, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(margin_top_bottom, margin_left_right+5, margin_top_bottom, 0));

        GridPane.setConstraints(widthLabel, 1, 3, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.ALWAYS, new Insets(margin_top_bottom, 0, margin_top_bottom, margin_left_right));
        GridPane.setConstraints(width, 2, 3, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(margin_top_bottom, margin_left_right, margin_top_bottom, 0));
        GridPane.setConstraints(heightLabel, 3, 3, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.ALWAYS, new Insets(margin_top_bottom, 0, margin_top_bottom, margin_left_right));
        GridPane.setConstraints(height, 4, 3, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(margin_top_bottom, margin_left_right+5, margin_top_bottom, 0));

        pane.getChildren().addAll(titledPaneSpot,roiLabel,xPositionLabel,xStartPosition,yPositionLabel,yStartPosition,widthLabel,width,heightLabel,height);

        pane.setMinWidth(0);
        pane.setMinHeight(0);

        int[][] g = RXTomoJ.getInstance().getModelController().getDataController().getReductionInfo().getTransmissionROI();
        update(g);

        return pane;
    }


    public void update(int[][] roi){
        int[] r = new int[4];
        r[0] = roi[0][0];
        r[1] = roi[1][0];
        r[2] = roi[0][1]-roi[0][0];
        r[3] = roi[1][1]-roi[1][0];

        xStartPosition.setText(r[0]+"");
        yStartPosition.setText(r[1]+"");
        width.setText(r[2]+"");
        height.setText(r[3]+"");
    }
}
