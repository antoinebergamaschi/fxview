package com.soleil.nanoscopium.FXview.accordion.option;

import com.soleil.nanoscopium.FXview.content.FluoContentPane;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.Pane;

/**
 * Created by bergamaschi on 03/08/2015.
 */
public class SpectrumOption extends AccordionOptionalPane<FluoContentPane> {

    public SpectrumOption(FluoContentPane view) {
        super(view);
    }

    @Override
    protected ToggleButton getDefaultIcon() {
        ToggleButton arrowLeft = new ToggleButton("",arrowIcon);

        arrowIcon.setIcon(FontAwesomeIcon.COG);

        return arrowLeft;
    }

    @Override
    public String getTitle() {
        return "Test MF";
    }

    @Override
    protected Pane getDefaultPane() {
        Pane pane = new Pane();
        //Add the Log Button
        CheckBox box = new CheckBox("Log");
        box.setSelected(true);
        box.setOnAction(l -> {
            view.setLog(box.isSelected());
        });
        pane.getChildren().add(box);

        //Add a small Selection pane to choose custom spectrum color, save the spectrum
        // load fit files...



        return pane;
    }
}
