package com.soleil.nanoscopium.FXview.accordion.option;

import com.soleil.nanoscopium.FXview.content.FluoContentPane;
import com.soleil.nanoscopium.FXview.utils.FXutils;
import com.soleil.nanoscopium.nistloader.Elements;
import com.soleil.nanoscopium.nistloader.NistData;
import com.soleil.nanoscopium.nistloader.TransitionType;
import com.soleil.nanoscopium.nistloader.view.ElementTableListener;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;

/**
 * Created by bergamaschi on 05/08/2015.
 */
public class ElementInformation extends AccordionOptionalPane<FluoContentPane> implements ElementTableListener{
    protected ElementInformation(FluoContentPane view) {
        super(view);
    }

    private GridPane info = new GridPane();

    @Override
    protected ToggleButton getDefaultIcon() {
        ToggleButton arrowLeft = new ToggleButton("",arrowIcon);
        arrowIcon.setIcon(FontAwesomeIcon.TH);
        return arrowLeft;
    }

    @Override
    public String getTitle() {
        return "Test MF";
    }

    @Override
    protected Pane getDefaultPane() {
        info.getStyleClass().add("table");
        info.setMinWidth(0);
        info.setMinHeight(0);
        return info;
    }

    @Override
    public void selected(boolean b, Elements elements) {

    }

    @Override
    public void hover(Elements elements) {
        int i = 0;
        int j = 0;
        info.getChildren().clear();


        Label elementName = new Label(elements.getFullName());
        elementName.setMinWidth(0);
        elementName.setMinWidth(0);

        if ( NistData.getData(elements) != null ) {
            int size = NistData.getData(elements).getFluorescenceLine().size() / 4 * 2;

            //Add Columns name
            Label transitionColName = new Label(FXutils.getLang("accordionPane.ElementInformation.transitionName"));
            Label transitionColEn = new Label(FXutils.getLang("accordionPane.ElementInformation.transitionEnergy"));

            GridPane.setConstraints(elementName, j, i,  size , 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 0));
            i++;

            GridPane.setConstraints(transitionColName, j, i,  1 , 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 0));
            GridPane.setConstraints(transitionColEn, j+1, i,  1 , 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 0));
            i++;
            info.getChildren().addAll(transitionColName, transitionColEn);

            for (TransitionType transitionType : NistData.getData(elements).getTransitionOrdered()) {
                Label transitionName = new Label(transitionType.name());
                Label transitionValue = new Label(NistData.getData(elements).getFluorescenceLine().get(transitionType)[0] + "/("+ NistData.getData(elements).getFluorescenceLine().get(transitionType)[1]+")");

                transitionName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
                transitionValue.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
                transitionName.setAlignment(Pos.CENTER);
                transitionValue.setAlignment(Pos.CENTER);

                GridPane.setConstraints(transitionName, j, i, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));
                GridPane.setConstraints(transitionValue, j+1, i, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));

                if ( i > 6 ) {
                    j+=2;
                    i = 1;
                    //Add Columns name
                    transitionColName = new Label(FXutils.getLang("accordionPane.ElementInformation.transitionName"));
                    transitionColEn = new Label(FXutils.getLang("accordionPane.ElementInformation.transitionEnergy"));

                    GridPane.setConstraints(transitionColName, j, i,  1 , 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 0));
                    GridPane.setConstraints(transitionColEn, j+1, i,  1 , 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 0));
                    i++;
                    info.getChildren().addAll(transitionColName, transitionColEn);
                }else{
                    i++;
                }


                transitionName.getStyleClass().add("tableElement");
                transitionValue.getStyleClass().add("tableElement");

                info.getChildren().addAll(transitionName, transitionValue);
            }
        }else{
            GridPane.setConstraints(elementName, j, i,  1 , 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 0));
            i++;

            Label errorNoInfo = new Label(FXutils.getLang("accordionPane.MendeleevOption.errorNoData"));
            GridPane.setConstraints(errorNoInfo, 0, i, 2, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 0));
            info.getChildren().add(errorNoInfo);
        }

        info.getChildren().add(elementName);
        info.requestLayout();
    }
}
