package com.soleil.nanoscopium.FXview.accordion;

import com.soleil.nanoscopium.FXview.accordion.option.AccordionOptionalPane;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.*;

/**
 * Created by bergamaschi on 07/08/2015.
 */
public class OptionBarV extends OptionBar{

    public OptionBarV(AccordionPane accordionPane, Pane container){
        super(accordionPane, container);
        this.getStyleClass().add("V");
        this.setPrefWidth(AccordionPane.optionBarSize);
        this.setMinWidth(AccordionPane.optionBarSize);
        this.setMaxWidth(AccordionPane.optionBarSize);
    }

    @Override
    public void addToBar(AccordionOptionalPane accordionOptionalPane){
        accordionOptionalPanes.add(accordionOptionalPane);
        ToggleButton icon = accordionOptionalPane.getIcon();
        addToggleButton(icon);
        icon.setText(accordionOptionalPane.getTitle());
        icon.getStyleClass().add("accordion-option-bar-title-V");

        icon.setMinWidth(USE_COMPUTED_SIZE);
        icon.setMinHeight(USE_COMPUTED_SIZE);

        Group group = new Group();
        group.getChildren().addAll(icon);

        GridPane.setConstraints(group, 0, this.getChildren().size(), 1, 1, HPos.LEFT, VPos.TOP, Priority.NEVER, Priority.ALWAYS, new Insets(0, 0, 0, 0));
        this.getChildren().add(group);

        //Open or close the LeftPanel with the panel
        icon.setOnMouseClicked(l->{
            if ( icon.isSelected() ) {
//                   If the AccordionPane do not contains already the leftContentNode
                if ( !accordionPane.getChildren().contains(container) ) {
                    accordionPane.getChildren().add(container);

                    ScrollPane scrollPane = accordionOptionalPane.getPane();

                    scrollPane.setFitToHeight(true);

                    AnchorPane.setBottomAnchor(scrollPane, 0.0d);
                    AnchorPane.setTopAnchor(scrollPane, 0.0d);
                    AnchorPane.setRightAnchor(scrollPane, 0.0d);
                    AnchorPane.setLeftAnchor(scrollPane, 0.0d);


                    container.widthProperty().addListener(w -> {
                        //TODO -4?? border layout ?
                        ((Region) scrollPane.getContent()).setPrefWidth(container.getWidth() - 4);
                    });

                    container.getChildren().add(scrollPane);

                    AccordionTransition accordionTransition = new AccordionTransition(container, containerSize, AccordionTransition.AnimationDirection.WEST);
                    accordionTransition.play();
                    accordionPane.reconfOptionBar();
                }
                //Else clear the content inside BottomContent
                else{
                    container.getChildren().clear();
                    container.getChildren().add(accordionOptionalPane.getPane());
                }
            }else{
                AccordionTransition accordionTransition = new AccordionTransition(container,containerSize, AccordionTransition.AnimationDirection.EST);
                accordionTransition.play();
                accordionTransition.setOnFinished(k -> {
                    container.getChildren().clear();
                    //Remove the pane to completely remove visually the pane
                    accordionPane.getChildren().remove(container);
                    accordionPane.reconfOptionBar();
                });
            }
        });
    }
}
