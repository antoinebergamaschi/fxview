package com.soleil.nanoscopium.FXview.accordion;

import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

/**
 * Created by bergamaschi on 07/08/2015.
 */
public class AccordionTransition extends Transition {

    public enum AnimationDirection{
        NORTH,SOUTH,EST,WEST
    }

    final private Duration animationTime = Duration.seconds(1);
    private Pane nodeToresize;
    private double maxLenght;
    private AnimationDirection animationDirection;

    public AccordionTransition(Pane node, double size, AnimationDirection animationDirection){
        super();
        setInterpolator(Interpolator.LINEAR);
        setCycleCount(1);
        setCycleDuration(animationTime);

        this.nodeToresize = node;
        this.maxLenght = size;
        this.animationDirection = animationDirection;

    }

    @Override
    protected void interpolate(double frac) {
        double curWidth = 0;
        switch (animationDirection){
            case WEST:
                curWidth = maxLenght * frac;
                break;
            case EST:
                curWidth = maxLenght * (1-frac);
                break;
            case NORTH:
                curWidth = maxLenght * frac;
                break;
            case SOUTH:
                curWidth = maxLenght * (1-frac);
                break;
        }

        switch (animationDirection){
            case WEST:
            case EST:
                nodeToresize.setMinWidth(curWidth);
                nodeToresize.setPrefWidth(curWidth);
                break;
            case NORTH:
            case SOUTH:
                nodeToresize.setPrefHeight(curWidth);
                nodeToresize.setMinHeight(curWidth);
                break;
        }
    }

}
