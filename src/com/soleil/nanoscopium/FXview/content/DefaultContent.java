package com.soleil.nanoscopium.FXview.content;

import com.soleil.nanoscopium.FXview.content.label.ButtonContent;
import com.soleil.nanoscopium.FXview.utils.FXutils;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;

/**
 * Created by bergamaschi on 15/04/2015.
 */
public class DefaultContent extends DefaultContentPane {

    @Override
    protected Node getContentPane() {
        return new Label("test");
    }

    @Override
    protected ButtonContent getToggleButton() {
        Image default_icon = new Image(FXutils.getResource("MMXI.icon.default.home"));
        Image hover_icon = new Image(FXutils.getResource("MMXI.icon.hover.home"));
        Image selected_icon = new Image(FXutils.getResource("MMXI.icon.selected.home"));

        ButtonContent first  = new ButtonContent(FXutils.getLang("contentPane.DefaultContent.defaultStatus"),default_icon,hover_icon,selected_icon,null);

//        first.getStyleClass().add("firstToggleButton");

        return first;
    }

    @Override
    protected String getDefaultStatus() {
        return "test";
    }

    @Override
    protected void modelEvent(MMXIControllerEvent mmxiControllerEvent) {
        System.err.println("modelEvent Update not implemented :: "+this.getClass());
    }

    @Override
    void update() {
        System.err.println("Function Update Not implemented "+this.getClass());
    }
}
