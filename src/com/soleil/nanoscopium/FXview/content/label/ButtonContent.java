package com.soleil.nanoscopium.FXview.content.label;

import com.soleil.nanoscopium.FXview.event.HighLightEvent;
import com.soleil.nanoscopium.FXview.event.HighLightListener;
import javafx.scene.Node;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.ArrayList;

/**
 * Created by antoine bergamaschi on 13/04/2015.
 */
public class ButtonContent extends ToggleButton implements HighLightEvent{

    private ArrayList<HighLightListener> highLightListeners = new ArrayList<>();
    private String defaultClass = "labelContent";
    
    private int height = 75;
    private int width = 75;

    private boolean isHightlighted = false;

    public ButtonContent(String title){
        this(title,null);
    }

    public ButtonContent(String title, Node graphic){
        super(title, graphic);
        this.getStyleClass().addAll(defaultClass);
        this.setPrefSize(width, height);
    }

    public ButtonContent(String title, Image default_icon, Image hover_icon, Image selected_icon, Image disabled_icon){
        if ( default_icon != null ) {

            ImageView imageView = new ImageView();
            imageView.setSmooth(true);
            imageView.setPreserveRatio(true);
            imageView.prefHeight(height);
            imageView.prefWidth(width);
            imageView.setImage(default_icon);

            Tooltip tooltip = new Tooltip(title);
            this.setTooltip(tooltip);
            this.setGraphic(imageView);

            if ( disabled_icon != null ) {
                this.disableProperty().addListener(l -> {
                    if (this.isDisabled()) {
                        imageView.setImage(disabled_icon);
                    } else {
                        imageView.setImage(default_icon);
                    }
                });
            }

            if (selected_icon != null) {
                this.selectedProperty().addListener(l -> {
                    if (this.isSelected()) {
                        imageView.setImage(selected_icon);
                    } else {
                        if (this.isHover()) {
                            imageView.setImage(hover_icon);
                        } else {
                            imageView.setImage(default_icon);
                        }
                    }
                });
            }

            if ( hover_icon != null ) {
                this.hoverProperty().addListener(l -> {
                    if (!this.isSelected()) {
                        if (this.isHover()) {
                            imageView.setImage(hover_icon);
                        } else {
                            imageView.setImage(default_icon);
                        }
                    }
                });
            }


            this.getStyleClass().add(defaultClass);
            this.setPrefSize(width, height);
        }
    }

    public ButtonContent(String title, Image default_icon, Image hover_icon, Image selected_icon, Image disabled_icon, Image highlight_icon){
        this(title, default_icon, hover_icon, selected_icon, disabled_icon);

        if ( highlight_icon != null ) {
            this.addHighLightListener(l -> {
                if (!this.isSelected()) {
                    if (l) {
                        ((ImageView)this.getGraphic()).setImage(highlight_icon);
                    } else {
                        ((ImageView)this.getGraphic()).setImage(default_icon);
                    }
                }
            });
        }
    }

    @Override
    public void addHighLightListener(HighLightListener highLightListener) {
        this.highLightListeners.add(highLightListener);
    }

    @Override
    public void removeHighLightListener() {
        this.highLightListeners = new ArrayList<>();
    }


    public boolean isHightlighted() {
        return isHightlighted;
    }

    public void setIsHightlighted(boolean isHightlighted) {
        this.isHightlighted = isHightlighted;
        fireHighLight(isHightlighted);
    }

    public void fireHighLight(boolean isHightlighted){
        this.highLightListeners.forEach(k->k.highLight(isHightlighted));
    }
}
