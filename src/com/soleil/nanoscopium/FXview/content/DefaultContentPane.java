package com.soleil.nanoscopium.FXview.content;

import com.soleil.nanoscopium.FXview.content.label.ButtonContent;
import com.soleil.nanoscopium.FXview.event.*;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerUpdatedListener;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.Node;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ToggleButton;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by bergamaschi on 14/04/2015.
 */
public abstract class DefaultContentPane implements ContentPane, StatusChange,
        MMXIControllerUpdatedListener, Notification, CustomError {
    protected ArrayList<StatusListener> statusListeners = new ArrayList<>();
    protected ArrayList<NotificationListener> notificationListeners = new ArrayList<>();
    protected ArrayList<ErrorListener> errorListeners = new ArrayList<>();


    private AnchorPane loadingPane;
    private ButtonContent buttonContent;

    public DefaultContentPane(){

    }

    @Override
    public void addStatusListener(StatusListener statusListener) {
        statusListeners.add(statusListener);
    }

    @Override
    public void removeStatusListener() {
        statusListeners.clear();
    }

    @Override
    public void addNotificationListener(NotificationListener notificationListener) {
        notificationListeners.add(notificationListener);
    }

    @Override
    public void removeNotificationListener() {
        notificationListeners.clear();
    }

    @Override
    public void addErrorListener(ErrorListener errorListener) {
        errorListeners.add(errorListener);
    }

    @Override
    public void removeAllErrorListener() {
        errorListeners.clear();
    }

    @Override
    public void removeErrorListener(ErrorListener errorListener) {
        errorListeners.remove(errorListener);
    }

    public void fireStatusUpdate(String text, double process){
        statusListeners.forEach(s -> s.statusChange(text, process));
    }

    public void fireNotification(String text){
        notificationListeners.forEach(n -> n.notification(text));
    }

    public void fireError(String text){
        errorListeners.forEach(f->f.error(text));
    }

    protected abstract Node getContentPane();
    protected abstract ButtonContent getToggleButton();
    protected abstract String getDefaultStatus();

    @Override
    public Node getContent() {
        Node node = getContentPane();

        node.getStyleClass().add("contentPaneDefault");

        //Init the loading pane
        createLoadingPane();

        //Add a new pane
        AnchorPane pane = new AnchorPane();
        AnchorPane.setBottomAnchor(node, 0.0);
        AnchorPane.setTopAnchor(node, 0.0);
        AnchorPane.setRightAnchor(node, 0.0);
        AnchorPane.setLeftAnchor(node, 0.0);

        AnchorPane.setBottomAnchor(loadingPane, 0.0);
        AnchorPane.setTopAnchor(loadingPane, 0.0);
        AnchorPane.setRightAnchor(loadingPane, 0.0);
        AnchorPane.setLeftAnchor(loadingPane, 0.0);


        pane.getChildren().addAll(node, loadingPane);

        hideLoading();

        //Fire Update of the status
        fireStatusUpdate(getDefaultStatus(), -1);

        //Fire first Update
        update();

        return pane;
    }

    @Override
    public ToggleButton getButton() {
        buttonContent = getToggleButton();
        //Define Custom Event on the Toggle Buttons

        //Special animation Border Fade In
        DropShadow dropShadowMouseEnter = new DropShadow();
        dropShadowMouseEnter.setOffsetY(0.0);
        dropShadowMouseEnter.setOffsetX(0.0);
        dropShadowMouseEnter.setBlurType(BlurType.GAUSSIAN);
        dropShadowMouseEnter.setColor(Color.TRANSPARENT);

        final Timeline timelineHighLight = new Timeline();
        timelineHighLight.setCycleCount(1);
        timelineHighLight.setAutoReverse(true);
        final KeyValue kv1 = new KeyValue(dropShadowMouseEnter.colorProperty(), Color.YELLOW);
        final KeyFrame kf1 = new KeyFrame(Duration.millis(100), kv1);
        timelineHighLight.getKeyFrames().add(kf1);

        final Timeline timelineMouseEnter = new Timeline();
        timelineMouseEnter.setCycleCount(1);
        timelineMouseEnter.setAutoReverse(true);
        final KeyValue kv = new KeyValue(dropShadowMouseEnter.colorProperty(), Color.DARKSLATEGREY);
        final KeyFrame kf = new KeyFrame(Duration.millis(100), kv);
        timelineMouseEnter.getKeyFrames().add(kf);


        buttonContent.setOnMouseMoved(k -> {
            fireStatusUpdate(getDefaultStatus(), -1);
        });

        buttonContent.setOnMouseEntered(k -> {
            buttonContent.setEffect(dropShadowMouseEnter);
            timelineMouseEnter.play();
        });

        buttonContent.setOnMouseExited(k -> {
            buttonContent.setEffect(null);
            dropShadowMouseEnter.setColor(Color.TRANSPARENT);
            timelineMouseEnter.stop();
        });


        buttonContent.addHighLightListener(h -> {
            if (h) {
                buttonContent.setEffect(dropShadowMouseEnter);
                dropShadowMouseEnter.setColor(Color.TRANSPARENT);
                timelineHighLight.play();
            }
        });

        return buttonContent;
    }

    @Override
    public void mmxiControllerUpdated(MMXIControllerEvent mmxiControllerEvent) {
        if ( buttonContent != null && buttonContent.isSelected() ) {
            //Fire Event to the child Display
//            System.out.println("An "+mmxiControllerEvent.getEventType().toString()+" Occurs :: "+mmxiControllerEvent.getEventID().toString()+" ... "+new Date().getTime());
            modelEvent(mmxiControllerEvent);
        }
    }

    abstract protected void modelEvent(MMXIControllerEvent mmxiControllerEvent);

    @Override
    protected void finalize() throws Throwable {
        //Remove this Listener from the Model
        RXTomoJ.getInstance().getModelController().removeMMXIControllerUpdatedListener(this);
        super.finalize();
    }

    abstract void update();


    public ButtonContent getButtonContent() {
        return buttonContent;
    }

    private void createLoadingPane(){
        loadingPane = new AnchorPane();
        ProgressIndicator progressIndicator = new ProgressIndicator();
        loadingPane.toFront();
        loadingPane.getStyleClass().add("mmxi-progress-indicator");

        AnchorPane.setBottomAnchor(progressIndicator, loadingPane.heightProperty().getValue() / 4.0d);
        AnchorPane.setTopAnchor(progressIndicator, loadingPane.heightProperty().getValue() / 4.0d);
        AnchorPane.setRightAnchor(progressIndicator, loadingPane.widthProperty().getValue() / 4.0d);
        AnchorPane.setLeftAnchor(progressIndicator, loadingPane.widthProperty().getValue() / 4.0d);

        loadingPane.widthProperty().addListener(l -> {
            AnchorPane.setRightAnchor(progressIndicator, loadingPane.widthProperty().getValue() / 4.0d);
            AnchorPane.setLeftAnchor(progressIndicator, loadingPane.widthProperty().getValue() / 4.0d);
        });

        loadingPane.heightProperty().addListener(l->{
            AnchorPane.setBottomAnchor(progressIndicator, loadingPane.heightProperty().getValue() / 4.0d);
            AnchorPane.setTopAnchor(progressIndicator, loadingPane.heightProperty().getValue() / 4.0d);
        });

        loadingPane.getChildren().add(progressIndicator);
    }

    public void displayLoading(){
        loadingPane.setVisible(true);
    }

    public void hideLoading(){
        loadingPane.setVisible(false);
    }
}
