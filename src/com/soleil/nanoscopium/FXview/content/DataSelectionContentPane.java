package com.soleil.nanoscopium.FXview.content;

import com.soleil.nanoscopium.FXview.Modal.popup.SelectDataPopUp;
import com.soleil.nanoscopium.FXview.content.label.ButtonContent;
import com.soleil.nanoscopium.FXview.utils.FXutils;
import com.soleil.nanoscopium.hdf5Opener.Hdf5Handler;
import com.soleil.nanoscopium.rximage.RXData;
import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.RXSpectrum;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.ComputationController;
import com.soleil.nanoscopium.rxtomoj.controller.DataController;
import com.soleil.nanoscopium.rxtomoj.controller.SessionController;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import com.soleil.nanoscopium.rxtomoj.event.MMXIEventData;
import com.soleil.nanoscopium.rxtomoj.model.SessionObject;
import com.soleil.nanoscopium.rxtomoj.model.data.StackData;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.MotorType;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;
import de.jensd.fx.glyphs.*;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.geometry.*;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by bergamaschi on 27/04/2015.
 */
public class DataSelectionContentPane extends DefaultContentPane {

    private UUID currentUUID;

    private ContentData imageContent ;
    private ContentData saiContent ;
    private ContentData motorContent ;
    private ContentData spectrumContent ;

    //Hash map containing Hdf5 data, updated in the Update code
    private HashMap<String, Hdf5Handler.Dataset_Iterate_cb_o1.DatasetObject> hashMap;

    @Override
    protected Node getContentPane() {
        //Create a TabPane
        TabPane tabPane = new TabPane();
        tabPane.setSide(Side.TOP);
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

        //First Pane is for HDF5 information
        Tab tabHdf = new Tab(FXutils.getLang("contentPane.DataSelectionContentPane.Tab.hdf"));
        //Second for Image created or imported
        Tab tabImage = new Tab(FXutils.getLang("contentPane.DataSelectionContentPane.Tab.image"));

        tabPane.getTabs().addAll(tabHdf,tabImage);

        tabHdf.setContent(createHdf5Pane());
        tabImage.setContent(buildImageInterface());

        return tabPane;
    }

    @Override
    protected ButtonContent getToggleButton() {
        Image default_icon = new Image(FXutils.getResource("MMXI.icon.default.selection"));
        Image hover_icon = new Image(FXutils.getResource("MMXI.icon.hover.selection"));
        Image selected_icon = new Image(FXutils.getResource("MMXI.icon.selected.selection"));
        Image disabled_icon = new Image(FXutils.getResource("MMXI.icon.disabled.selection"));

        ButtonContent fluo  = new ButtonContent("Data Selection",default_icon,hover_icon,selected_icon,disabled_icon);
        return fluo;
    }

    @Override
    protected String getDefaultStatus() {
        return FXutils.getLang("contentPane.DataSelectionContentPane.defaultStatus");
    }

    /**
     * Build the Hdf5 Pane interface
     * @return The Hdf5 Pane interface
     */
    private GridPane createHdf5Pane(){
        GridPane gridPane = new GridPane();

        gridPane.getStyleClass().addAll("dataSelectionContentPane");

        imageContent = new ContentData(FXutils.getLang("contentPane.DataSelectionContentPane.image.title"),ContentData.IMAGE);
        saiContent = new ContentData(FXutils.getLang("contentPane.DataSelectionContentPane.sai.title"),ContentData.SAI);
        motorContent = new ContentData(FXutils.getLang("contentPane.DataSelectionContentPane.motor.title"),ContentData.MOTORS);
        spectrumContent = new ContentData(FXutils.getLang("contentPane.DataSelectionContentPane.spectrum.title"),ContentData.FLUORESCENCE);


        motorContent.addLineData(FXutils.getLang("contentPane.DataSelectionContentPane.motor.MOTOR_X_name"),FXutils.getLang("contentPane.DataSelectionContentPane.motor.void"),false);
        motorContent.addLineData(FXutils.getLang("contentPane.DataSelectionContentPane.motor.MOTOR_Y_name"),FXutils.getLang("contentPane.DataSelectionContentPane.motor.void"),false);
        motorContent.addLineData(FXutils.getLang("contentPane.DataSelectionContentPane.motor.MOTOR_Z_name"),FXutils.getLang("contentPane.DataSelectionContentPane.motor.void"), false);
        motorContent.addLineData(FXutils.getLang("contentPane.DataSelectionContentPane.motor.MOTOR_R_name"),FXutils.getLang("contentPane.DataSelectionContentPane.motor.void"), false);
        motorContent.setAddable(false);

//        GridPane gridpane = new GridPane();
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(50);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(50);
        gridPane.getColumnConstraints().addAll(col1, col2);

        GridPane.setConstraints(imageContent, 0, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.ALWAYS, new Insets(0, 0, 0, 0));
        GridPane.setConstraints(spectrumContent, 1, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.ALWAYS, new Insets(0, 0, 0, 0));
        GridPane.setConstraints(saiContent, 0, 1, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.ALWAYS, new Insets(0, 0, 0, 0));
        GridPane.setConstraints(motorContent, 1, 1, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.ALWAYS, new Insets(0, 0, 0, 0));

        gridPane.getChildren().addAll(imageContent, saiContent, motorContent, spectrumContent);

        return gridPane;
    }

    private GridPane buildImageInterface(){
        GridPane gridPane = new GridPane();

        int i = 0;
        //Create an entry interface for every basic Type
        for ( StackType stackType : StackType.getBasicType()){
            LineImageData lineImageData  = new LineImageData(stackType);
            GridPane.setConstraints(lineImageData, 0, i, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));
            gridPane.getChildren().add(lineImageData);
            i++;
        }

        return gridPane;
    }

    /**
     * Contains LineData
     */
    private class ContentData extends GridPane{
        private final static int IMAGE=0,SAI=1,FLUORESCENCE=2,MOTORS=3;

        /**
         * Identifier of this ContentData
         * Each content data has one Data type and update the model depending of this type
         */
        private int identifier = -1 ;

        /**
         * Last Position Where Data may be add in the Grid
         */
        private int currentPosition = 1;

        private Text plusIcon;

        public ContentData(String title, int id){
           this(title);
           this.identifier = id;
        }

        public ContentData(String title){
            this.getStyleClass().add("contentData");

            AnchorPane pane = new AnchorPane();

            Label label = new Label(title);
            AnchorPane.setBottomAnchor(label, 0.0d);
            AnchorPane.setTopAnchor(label, 0.0d);
            AnchorPane.setLeftAnchor(label, 0.0d);
            AnchorPane.setRightAnchor(label, 0.0d);

            pane.getChildren().addAll(label);
            pane.getStyleClass().add("title");

            label.setAlignment(Pos.CENTER);

            GridPane.setConstraints(pane, 0, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 0));


            plusIcon = GlyphsDude.createIcon(FontAwesomeIcon.PLUS,FXutils.getResourceAsString("MMXI.icon.default.size"));
            plusIcon.getStyleClass().add("fontawesomeIconDefault");

            plusIcon.setOnMouseClicked(l->{
                SelectDataPopUp sel;
                switch (identifier) {
                    case IMAGE:
                        sel = new SelectDataPopUp(RXTomoJ.getInstance().getModelController().getSessionController().getDataset(SessionObject.DataType.IMAGE), hashMap);
                        RXTomoJ.getInstance().getModelController().getSessionController().manageDataset(SessionObject.DataType.IMAGE, sel.getData());
                        break;
                    case SAI:
                        sel = new SelectDataPopUp(RXTomoJ.getInstance().getModelController().getSessionController().getDataset(SessionObject.DataType.XBPM), hashMap);
                        RXTomoJ.getInstance().getModelController().getSessionController().manageDataset(SessionObject.DataType.XBPM, sel.getData());
                        break;
                    case FLUORESCENCE:
                        sel = new SelectDataPopUp(RXTomoJ.getInstance().getModelController().getSessionController().getDataset(SessionObject.DataType.SPECTRUM), hashMap);
                        RXTomoJ.getInstance().getModelController().getSessionController().manageDataset(SessionObject.DataType.SPECTRUM, sel.getData());
                        break;
                    default:
                        fireError(FXutils.getLang("contentPane.DataSelectionContentPane.ERROR.gridType"));
                        break;
                }
            });

            GridPane.setConstraints(plusIcon, 0, currentPosition, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));



            this.getChildren().addAll(pane, plusIcon);
        }

        /**
         * Add A line Data
         * @param title The Title to set on this data
         * @param contentName The String to display
         */
        public void addLineData(String title, String contentName, boolean removable){
            LineData lineData = new LineData(title,contentName, this, removable);
            GridPane.setConstraints(lineData, 0, currentPosition, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));
            currentPosition++;
            GridPane.setConstraints(plusIcon, 0, currentPosition, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));
            if ( contentName != FXutils.getLang("contentPane.DataSelectionContentPane.motor.void") ) {
                lineData.disable(false);
            }
            this.getChildren().add(lineData);
        }

        public void removeAllDataLine(){
            this.getChildren().remove(1,getChildren().size());
            currentPosition = getChildren().size();
            GridPane.setConstraints(plusIcon, 0, currentPosition, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));
            this.getChildren().add(plusIcon);
        }

        public void setAddable(boolean addable){
            plusIcon.setVisible(addable);
        }


        public int getIdentifier(){
            return this.identifier;
        }

    }

    private class LineData extends GridPane{

        private CheckBox checkBox;
        private Label imageName;

        public LineData(){
            this("");
        }

        public LineData(String title){
            this(title,"",null);
        }


        public LineData(String title, String content, ContentData contentData){
            this(title,content,contentData,true);
        }

        public LineData(String title, String content, ContentData contentData, boolean removable){

            this.getStyleClass().add("lineData");
            Label label = new Label(title);
            label.getStyleClass().add("title");
            GridPane gridPane = new GridPane();
            imageName = new Label(content);
            checkBox = new CheckBox();

            //If this Line is removable
            if ( removable ) {
                Region fontAwesomeIcon = GlyphsStack.create()
                        .add(GlyphsBuilder.create(FontAwesomeIconView.class)
                                .styleClass("square_close")
                                .build())
                        .add(GlyphsBuilder.create(FontAwesomeIconView.class)
                                        .styleClass("times_close")
                                        .build()
                        );


                fontAwesomeIcon.setOnMouseClicked(l -> {
                    if (contentData != null) {
                        RXTomoJ.getInstance().getModelController().getSessionController().removeRawDataset(SessionObject.DataType.IMAGE,imageName.getText());
//                        contentData.removeDataLine(this);
                    }
                });
                GridPane.setConstraints(fontAwesomeIcon, 0, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.ALWAYS, new Insets(0, 0, 0, 0));
                gridPane.getChildren().add(fontAwesomeIcon);
            }


            GridPane.setConstraints(imageName, 1, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 0));

            gridPane.getChildren().addAll(imageName);
            gridPane.getStyleClass().add("labelName");

            //Set Columns constraints
            if ( title == null || title.length() > 0) {
                ColumnConstraints col1 = new ColumnConstraints();
                col1.setPercentWidth(33);
                ColumnConstraints col2 = new ColumnConstraints();
                ColumnConstraints col3 = new ColumnConstraints();
                this.getColumnConstraints().addAll(col1, col2, col3);
            }

            GridPane.setConstraints(label, 0, 0, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));
            GridPane.setConstraints(gridPane, 1, 0, 1, 1, HPos.RIGHT, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 0));
            GridPane.setConstraints(checkBox, 2, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));

            this.getChildren().addAll(label, gridPane, checkBox);

            final long[] counter =  new long[1];
            //Add Listener, when clicking on the ImageName the content can be change

            //Change the Name when doubleClicking
            gridPane.setOnMouseClicked(l -> {
                if (counter[0] == 0) {
                    counter[0] = new Date().getTime();
                }else if ((new Date().getTime() - counter[0]) < TimeUnit.SECONDS.toMillis(1)){
                    SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
                    //Fire Double Click
                    SelectDataPopUp sel = new SelectDataPopUp(RXTomoJ.getInstance().getModelController().getSessionController().getDataset(SessionObject.DataType.IMAGE), hashMap);
                    if ( sel.isValid() && sel.getData() != null && sel.getData().size() > 0 ){
                        imageName.setText(sel.getData().get(0));

                        switch (contentData.getIdentifier()){
                            case ContentData.IMAGE:
                                sessionController.addRawDataset(SessionObject.DataType.IMAGE,sel.getData().get(0));
                                break;
                            case ContentData.FLUORESCENCE:
                                sessionController.addRawDataset(SessionObject.DataType.SPECTRUM, sel.getData().get(0));
                                break;
                            case ContentData.MOTORS:
                                sessionController.addRawDataset(SessionObject.DataType.MOTOR, sel.getData().get(0));
                                break;
                            case ContentData.SAI:
                                //Update the SAI
                                sessionController.addRawDataset(SessionObject.DataType.XBPM, sel.getData().get(0));
                                break;
                            default:
                                System.err.println("Wrong ID value :: "+this.getClass());
                        }

                        checkBox.setDisable(false);
                    }
                    else if ( sel.isReset()){
                        System.out.println("Commented Code :: l.354 "+this.getClass().toString());
//                        switch (contentData.getIdentifier()){
//                            case ContentData.IMAGE:
//                                sessionController.manageDataset(SessionObject.DataType.IMAGE, imageName.getText());
//                                break;
//                            case ContentData.FLUORESCENCE:
//                                sessionController.manageDataset(SessionObject.DataType.SPECTRUM, imageName.getText());
//                                break;
//                            case ContentData.MOTORS:
//                                sessionController.manageDataset(SessionObject.DataType.MOTOR, imageName.getText());
//                                imageName.setText(FXutils.getLang("contentPane.DataSelectionContentPane.motor.void"));
//                                checkBox.setSelected(false);
//                                break;
//                            case ContentData.SAI:
//                                //Update the SAI
//                                sessionController.manageDataset(SessionObject.DataType.XBPM, imageName.getText());
//                                break;
//                            default:
//                                System.err.println("Wrong ID value :: "+this.getClass());
//                        }

                        checkBox.setDisable(true);
                    }

                    counter[0] = 0;
                }
                else{
                    counter[0] = new Date().getTime();
                }
            });

            checkBox.setTooltip(new Tooltip(FXutils.getLang("contentPane.DataSelectionContentPane.check.tooltip")));

            checkBox.setOnMouseClicked(l->{
                DataController dataController  =  RXTomoJ.getInstance().getModelController().getDataController();

                switch (contentData.getIdentifier()){
                    case ContentData.IMAGE:
                        //Change the current image
                        dataController.setCurrentDataset(imageName.getText());
                        //Fire the Computation Thread to Init the ReductionInfo
                        RXTomoJ.getInstance().getModelController().getComputationController().spotMapInitialization();
                        break;
                    case ContentData.FLUORESCENCE:
                        //Update the fluorescence
                        dataController.manageSpectrum(imageName.getText());
                        break;
                    case ContentData.MOTORS:
                        //Test Which Motor has been modified
                        if ( title ==  FXutils.getLang("contentPane.DataSelectionContentPane.motor.MOTOR_X_name") ){
                            if ( checkBox.isSelected() && !imageName.getText().equalsIgnoreCase(content)){
                                //Add the motors with this value
                                dataController.manageMotor(MotorType.MOTOR_X,imageName.getText());
                            }else{
                                //Or remove the motor
                                dataController.manageMotor(MotorType.MOTOR_X,null);
                            }
                        }else if ( title ==  FXutils.getLang("contentPane.DataSelectionContentPane.motor.MOTOR_Y_name") ){
                            if ( checkBox.isSelected() && !imageName.getText().equalsIgnoreCase(content)){
                                //Add the motors with this value
                                dataController.manageMotor(MotorType.MOTOR_Y,imageName.getText());
                            }else{
                                //Or remove the motor
                                dataController.manageMotor(MotorType.MOTOR_Y,null);
                            }
                        }else if ( title ==  FXutils.getLang("contentPane.DataSelectionContentPane.motor.MOTOR_R_name") ){
                            if ( checkBox.isSelected() && !imageName.getText().equalsIgnoreCase(content)){
                                //Add the motors with this value
                                dataController.manageMotor(MotorType.MOTOR_R,imageName.getText());
                            }else{
                                //Or remove the motor
                                dataController.manageMotor(MotorType.MOTOR_R,null);
                            }
                        }else if ( title ==  FXutils.getLang("contentPane.DataSelectionContentPane.motor.MOTOR_Z_name") ){
                            if ( checkBox.isSelected() && !imageName.getText().equalsIgnoreCase(content)){
                                //Add the motors with this value
                                dataController.manageMotor(MotorType.MOTOR_Z,imageName.getText());
                            }else{
                                //Or remove the motor
                                dataController.manageMotor(MotorType.MOTOR_Z,null);
                            }
                        }
                        break;
                    case ContentData.SAI:
                        //Update the SAI
                        dataController.manageXBPM(imageName.getText());
                        break;
                    default:
                        System.err.println("Wrong ID value :: "+this.getClass());
                }
            });
        }

        public void setSelected(boolean sel){
            checkBox.setSelected(sel);
        }

        public void setImageName(String name){
            imageName.setText(name);
        }

        public void disable(boolean sel){
            checkBox.setDisable(sel);
        }
    }

    private class LineImageData extends GridPane{
        final private double counter[] = new double[1];
        private TextField textField = new TextField();
        private Label dimension = new Label("(...)");
        private StackType type;

        public LineImageData(StackType type){

            this.type = type;
            Label name = new Label(type.name());
            name.setLabelFor(textField);
            textField.setPromptText(FXutils.getLang("contentPane.DataSelectionContentPane.image.prompt"));

            GridPane.setConstraints(name, 0, 0, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(0, 5, 0, 5));
            GridPane.setConstraints(textField, 1, 0, 1, 1, HPos.LEFT, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 5));
            GridPane.setConstraints(dimension, 2, 0, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 5));

            this.getColumnConstraints().add(new ColumnConstraints(200));

            //Change the Name when doubleClicking
            textField.setOnMouseClicked(l -> {
                if (counter[0] == 0) {
                    counter[0] = new Date().getTime();
                } else if ((new Date().getTime() - counter[0]) < TimeUnit.SECONDS.toMillis(1)) {
                    //Fire Double Click
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle(FXutils.getLang("contentPane.DataSelectionContentPane.image.fileChooser"));
                    fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image Files", "*.tif", "*.tiff", "*.edf"));
                    File file = fileChooser.showOpenDialog(this.getScene().getWindow());
                    if (file != null) {
                        //Display the dimension of the image
                        textField.setText(file.getPath());
                        RXTomoJ.getInstance().getModelController().getDataController().storeStack(new StackData(type,file.getPath()));

                        int[] dims = new int[3];
                        //TODO test Here the change to StackData
                        RXData data = RXTomoJ.getInstance().getModelController().getDataController().getStorageData(new StackData(type));
                        if (data instanceof RXImage) {
                            dims = ((RXImage) data).getDims();
                        } else if (data instanceof RXSpectrum) {
                            dims = ((RXSpectrum) data).getDims();
                        }

                        String text = "[ ";
                        for (int i = 0; i < dims.length; i++) {
                            if (i < 2) {
                                text += dims[i] + ", ";
                            } else {
                                text += dims[i];
                            }
                        }
                        text += " ]";
                        dimension.setText(text);
                    }

                    counter[0] = 0;
                } else {
                    counter[0] = new Date().getTime();
                }
            });

            String path = RXTomoJ.getInstance().getModelController().getDataController().getStackPath(type);

            if ( path != null && !path.equalsIgnoreCase("")){
                textField.setText(path);
            }

            this.getChildren().addAll(name, textField, dimension);
        }
    }

    private void updateImagePane(){
        buildImageInterface();
    }

    private void updateHdfPane(){
        //Reset the Current Selection
        SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
        DataController dataController  =  RXTomoJ.getInstance().getModelController().getDataController();

        imageContent.removeAllDataLine();
        spectrumContent.removeAllDataLine();
        saiContent.removeAllDataLine();

        ArrayList<String> images = sessionController.getDataset(SessionObject.DataType.IMAGE);
        images.forEach(l -> {
            imageContent.addLineData("", l, true);
            if (l.equalsIgnoreCase(dataController.getDatasetName())) {
                ((LineData) imageContent.getChildren().get(imageContent.getChildren().size() - 1)).setSelected(true);
            }
        });

        ArrayList<String> spectrum = sessionController.getDataset(SessionObject.DataType.SPECTRUM);
        ArrayList<String> selSpectrum = dataController.getSelectedSpectrum();
        spectrum.forEach(l -> {
            spectrumContent.addLineData("", l, true);
            if (selSpectrum.contains(l)) {
                ((LineData) spectrumContent.getChildren().get(spectrumContent.getChildren().size() - 1)).setSelected(true);
            }
        });

        ArrayList<String> sais = sessionController.getDataset(SessionObject.DataType.XBPM);
        ArrayList<String> selSai = dataController.getSelectedXBPM();
        sais.forEach(l -> {
            saiContent.addLineData("", l, true);
            if (selSai.contains(l)) {
                ((LineData) saiContent.getChildren().get(saiContent.getChildren().size() - 1)).setSelected(true);
            }
        });

        ArrayList<String> motors = sessionController.getDataset(SessionObject.DataType.MOTOR);
        HashMap<MotorType, String> selMotor  =  dataController.getSelectedMotors();
        motors.forEach(l -> {
            for (MotorType type : selMotor.keySet() ){
                if ( selMotor.get(type).equalsIgnoreCase(l) ){
                    switch (type){
                        case MOTOR_R:
                            ((LineData) motorContent.getChildren().get(5)).setImageName(l);
                            ((LineData) motorContent.getChildren().get(5)).setSelected(true);
                            break;
                        case MOTOR_X:
                            ((LineData) motorContent.getChildren().get(2)).setImageName(l);
                            ((LineData) motorContent.getChildren().get(2)).setSelected(true);
                            break;
                        case MOTOR_Y:
                            ((LineData) motorContent.getChildren().get(3)).setImageName(l);
                            ((LineData) motorContent.getChildren().get(3)).setSelected(true);
                            break;
                        case MOTOR_Z:
                            ((LineData) motorContent.getChildren().get(4)).setImageName(l);
                            ((LineData) motorContent.getChildren().get(4)).setSelected(true);
                            break;
                    }
                }
            }
        });
    }

    @Override
    void update() {
        updateHdfPane();
        updateImagePane();

        if ( hashMap == null ){
            //Init the Hash Map
            this.displayLoading();
            String p = RXTomoJ.getInstance().getModelController().getSessionController().getPathToHdf5();
            Hdf5Handler hdf5Handler = new Hdf5Handler(p);
            hashMap = hdf5Handler.getDatasetInformation();
            this.hideLoading();
        }
    }

    @Override
    protected void modelEvent(MMXIControllerEvent mmxiControllerEvent) {
        MMXIEventData mmxiEventData = mmxiControllerEvent.getEventData();
        switch (mmxiControllerEvent.getEventType()) {
            case UPDATE:
                if ( mmxiControllerEvent.getEventID().equals(MMXIControllerEvent.MMXIControllerEventID.PROGRESS_FUNCTION) ){
                    if ( mmxiEventData.getID().equals(currentUUID) ){
                        fireStatusUpdate(FXutils.getLang("contentPane.DataSelectionContentPane.loadingStatus"),mmxiEventData.getValue());
                    }
                }else{
                    update();
                }
                break;
            case INITIALIZE:
                if ( mmxiEventData.getIdentifier() == ComputationController.SPOT_INITIALIZATION ){
                    //Ensure that Only One UUID is followed at one time
                    if ( currentUUID == null ) {
                        //Init Computation
                        currentUUID = mmxiEventData.getID();
                        displayLoading();
                    }
                }
                break;
            case FINISHED:
                if ( mmxiEventData.getIdentifier() == ComputationController.SPOT_INITIALIZATION ){
                    //Finilize Computation
                    currentUUID = null;
                    hideLoading();
                    fireStatusUpdate(getDefaultStatus(),-1);
                }
                break;
        }
    }
}
