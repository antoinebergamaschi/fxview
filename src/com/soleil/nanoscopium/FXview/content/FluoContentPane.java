package com.soleil.nanoscopium.FXview.content;

import com.soleil.nanoscopium.FXview.accordion.AccordionPane;
import com.soleil.nanoscopium.FXview.accordion.OptionBar;
import com.soleil.nanoscopium.FXview.accordion.option.MendeleevOption;
import com.soleil.nanoscopium.FXview.accordion.option.SpectrumOption;
import com.soleil.nanoscopium.FXview.content.label.ButtonContent;
import com.soleil.nanoscopium.FXview.utils.FXutils;
import com.soleil.nanoscopium.rximage.component.FX.RXSpectrumCanvas;
import com.soleil.nanoscopium.rximage.component.RXImageComponentFX;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.ComputationController;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import com.soleil.nanoscopium.rxtomoj.event.MMXIEventData;
import com.soleil.nanoscopium.rxtomoj.model.fluorescence.FluorescenceInfo;
import com.soleil.nanoscopium.rxtomoj.model.fluorescence.FluorescenceObject;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * Created by bergamaschi on 14/04/2015.
 */
public class FluoContentPane extends DefaultContentPane {
//    private SpectrumChart spectrumChart;
    private static boolean doUpdate = true;

    private RXImageComponentFX spectrumImage;
    private AccordionPane spectrumSelectionPanel;

    @Override
    protected String getDefaultStatus() {
        return FXutils.getLang("contentPane.FluoContentPane.defaultStatus");
    }

    @Override
    protected Node getContentPane() {
        
        GridPane gridPane = new GridPane();
//        spectrumChart = new SpectrumChart();
        //initiate with null means Spectrum
        spectrumImage = new RXImageComponentFX();

        ((RXSpectrumCanvas)spectrumImage.getImage()).setOption(false, true, false, true);

        spectrumSelectionPanel = new AccordionPane();
        spectrumSelectionPanel.setCenterContent(spectrumImage.getImage());

        spectrumSelectionPanel.addOption(OptionBar.OptionBarOrientation.VERTICAL, new SpectrumOption(this));
        spectrumSelectionPanel.addOption(OptionBar.OptionBarOrientation.HORIZONTAL,new MendeleevOption(this));

        GridPane.setConstraints(spectrumSelectionPanel, 0, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));

        gridPane.getChildren().addAll( spectrumSelectionPanel);

        return gridPane;
    }

    @Override
    protected ButtonContent getToggleButton() {
        Image default_icon = new Image(FXutils.getResource("MMXI.icon.default.fluo"));
        Image hover_icon = new Image(FXutils.getResource("MMXI.icon.hover.fluo"));
        Image selected_icon = new Image(FXutils.getResource("MMXI.icon.selected.fluo"));
        Image disabled_icon = new Image(FXutils.getResource("MMXI.icon.disabled.fluo"));

        ButtonContent fluo  = new ButtonContent("Fluorescence",default_icon,hover_icon,selected_icon,disabled_icon);
        return fluo;
    }


    @Override
    protected void modelEvent(MMXIControllerEvent mmxiControllerEvent) {
        MMXIEventData mmxiEventData = mmxiControllerEvent.getEventData();
        switch (mmxiControllerEvent.getEventID()){
            case PROGRESS_FUNCTION:
                switch (mmxiControllerEvent.getEventType()){
                    case FINISHED:
                        if ( mmxiEventData.getIdentifier() == ComputationController.COMPUTE_SUMSPECTRA ){
                            FluorescenceInfo fluorescenceInfo = RXTomoJ.getInstance().getModelController().getDataController().getFluorescenceInfo();
                            for (FluorescenceObject fluorescenceObject : fluorescenceInfo.getFluoData() ){
                                spectrumImage.addSpectrum(fluorescenceObject.getMeanSpectrum());
                            }

                            fireStatusUpdate(getDefaultStatus(),-1);

                            hideLoading();
                        }
                        break;
                    case UPDATE:
                        //Fire Update
                        fireStatusUpdate(FXutils.getLang("contentPane.FluoContentPane.loadingStatus"),mmxiEventData.getValue());
                        break;
                    case ERROR:
                        fireError("Error  "+this.getClass().toString());
                }
                break;
        }
    }

    @Override
    void update() {
        displayLoading();

        //If the spectrum have already been computed ask if recalculation is needed
        if ( RXTomoJ.getInstance().getModelController().getDataController().isSumSpectrumComputed() ){
            //Ask if need to recompute Spectrum
            Dialog<ButtonType> dialog = new Dialog<>();

            ButtonType buttonType = new ButtonType(FXutils.getLang("contentPane.FluoContentPane.dialog.recompute"), ButtonBar.ButtonData.YES);
            ButtonType buttonType2 = new ButtonType(FXutils.getLang("contentPane.FluoContentPane.dialog.keep"), ButtonBar.ButtonData.NO);
//            ButtonType buttonType1 = new ButtonType(FXutils.getLang("contentPane.FluoContentPane.dialog.keep2"), ButtonBar.ButtonData.RIGHT);

            dialog.setTitle(FXutils.getLang("contentPane.FluoContentPane.dialog.title"));
            dialog.setContentText(FXutils.getLang("contentPane.FluoContentPane.dialog.message"));

            dialog.getDialogPane().getButtonTypes().addAll(buttonType, buttonType2);
            dialog.showAndWait().ifPresent(response -> {
                if (response.getText().equalsIgnoreCase(FXutils.getLang("contentPane.FluoContentPane.dialog.keep"))) {
                    FluorescenceInfo fluorescenceInfo = RXTomoJ.getInstance().getModelController().getDataController().getFluorescenceInfo();
                    for (FluorescenceObject fluorescenceObject : fluorescenceInfo.getFluoData()) {
                        spectrumImage.addSpectrum(fluorescenceObject.getMeanSpectrum());
                    }
                    hideLoading();
                }
                else {
                    //Fire Event when finish that is read and traited
                    RXTomoJ.getInstance().getModelController().getComputationController().updateSumSpectra();
                }
            });
        }else{
            //Fire Event when finish that is read and traited
            RXTomoJ.getInstance().getModelController().getComputationController().updateSumSpectra();
        }
    }


    public void setLog(boolean log) {
        this.spectrumImage.setLog(log);
    }
}
