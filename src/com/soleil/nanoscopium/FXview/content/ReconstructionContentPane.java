package com.soleil.nanoscopium.FXview.content;

import com.soleil.nanoscopium.FXview.content.label.ButtonContent;
import com.soleil.nanoscopium.FXview.utils.FXutils;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import com.soleil.nanoscopium.rxtomoj.event.MMXIEventData;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Side;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by antoine bergamaschi on 03/09/2015.
 */
public class ReconstructionContentPane extends DefaultContentPane {

    @Override
    protected Node getContentPane() {
        //Create a TabPane
        TabPane tabPane = new TabPane();
        tabPane.setSide(Side.TOP);
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

        //First Pane is for HDF5 information
        Tab tabBP = new Tab(FXutils.getLang("contentPane.ReconstructionContentPane.Tab.bp"));
        //Second for Image created or imported
        Tab tabART = new Tab(FXutils.getLang("contentPane.ReconstructionContentPane.Tab.art"));

        tabPane.getTabs().addAll(tabBP,tabART);

        tabBP.setContent(buildFBPPane());
        tabART.setContent(buildARTPane());


        return tabPane;
    }

    @Override
    protected ButtonContent getToggleButton() {
        Image default_icon = new Image(FXutils.getResource("MMXI.icon.default.reconstruction"));
        Image hover_icon = new Image(FXutils.getResource("MMXI.icon.hover.reconstruction"));
        Image selected_icon = new Image(FXutils.getResource("MMXI.icon.selected.reconstruction"));
        Image disabled_icon = new Image(FXutils.getResource("MMXI.icon.disabled.reconstruction"));

        ButtonContent fluo  = new ButtonContent("Reconstruction",default_icon,hover_icon,selected_icon,disabled_icon);
        return fluo;
    }

    @Override
    protected String getDefaultStatus() {
        return FXutils.getLang("contentPane.ReconstructionContentPane.defaultStatus");
    }

    @Override
    protected void modelEvent(MMXIControllerEvent mmxiControllerEvent) {
        MMXIEventData mmxiEventData = mmxiControllerEvent.getEventData();
        switch (mmxiControllerEvent.getEventID()){
            case PROGRESS_FUNCTION:
                switch (mmxiControllerEvent.getEventType()){
                    case FINISHED:
                        fireStatusUpdate(getDefaultStatus(),-1);
                        hideLoading();
                        break;
                    case UPDATE:
                        //Fire Update
                        fireStatusUpdate(FXutils.getLang("contentPane.ReconstructionContentPane.loadingStatus"),mmxiEventData.getValue());
                        break;
                    case INITIALIZE:
                        displayLoading();
                        break;
                    case ERROR:
                        fireError("Error  " + this.getClass().toString());
                }
                break;
        }
    }

    @Override
    void update() {

    }

    private GridPane buildARTPane(){
        GridPane gridPane = new GridPane();

        ChoiceBox<StackType> dataToReconstruct = new ChoiceBox<>();
        ArrayList<StackType> current = RXTomoJ.getInstance().getModelController().getDataController().getBasicStack();

        for ( StackType stackType : current){
            dataToReconstruct.getItems().add(stackType);
        }

        if ( dataToReconstruct.getItems().size() > 0 ) {
            dataToReconstruct.getSelectionModel().select(0);
        }

        Label iteration = new Label(FXutils.getLang("contentPane.ReconstructionContentPane.art.iteration"));
        SpinnerValueFactory svf = new SpinnerValueFactory.IntegerSpinnerValueFactory(2, 100, 2, 1);
        Spinner iterationField = new Spinner();
        iterationField.setValueFactory(svf);
        iteration.setLabelFor(iterationField);

        Label relaxationCoef = new Label(FXutils.getLang("contentPane.ReconstructionContentPane.art.relaxation"));
        SpinnerValueFactory svf2 = new SpinnerValueFactory.DoubleSpinnerValueFactory(0.05, 1, 0.1, 0.05);
        Spinner relaxationField = new Spinner();
        relaxationField.setValueFactory(svf2);
        relaxationCoef.setLabelFor(relaxationField);


        Button button = new Button(FXutils.getLang("contentPane.ReconstructionContentPane.art.compute"));

        GridPane.setConstraints(dataToReconstruct, 0, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));

        GridPane.setConstraints(iteration, 0, 1, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));
        GridPane.setConstraints(iterationField, 1, 1, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));

        GridPane.setConstraints(relaxationCoef, 0, 2, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));
        GridPane.setConstraints(relaxationField, 1, 2, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));

        GridPane.setConstraints(button, 0, 3, 2, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));



        gridPane.getChildren().addAll(iteration,iterationField,relaxationCoef,relaxationField,button,dataToReconstruct);

        button.setOnAction(l->{
            System.out.println("This do Nothing :: l.135 "+this.getClass().toGenericString());
//            RXTomoJ.getInstance().getModelController().getDataController().setAlgebraicReconstructionInfo(dataToReconstruct.getValue(),Integer.parseInt(iterationField.getValue().toString()),Float.parseFloat(relaxationField.getValue().toString()),false,0);
            //Debug only
//            RXTomoJ.getInstance().getModelController().getDataController().setUseSinusoidalShift(false);
//            RXTomoJ.getInstance().getModelController().getReconstructionController().ART(dataToReconstruct.getValue());
        });


        return gridPane;
    }

    private GridPane buildFBPPane(){
        GridPane gridPane = new GridPane();


        return gridPane;
    }
}
