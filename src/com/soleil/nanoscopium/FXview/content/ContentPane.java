package com.soleil.nanoscopium.FXview.content;

import javafx.scene.Node;
import javafx.scene.control.ToggleButton;

/**
 * Created by bergamaschi on 14/04/2015.
 */
public interface ContentPane {
    ToggleButton getButton();
    Node getContent();
}
