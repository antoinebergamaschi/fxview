package com.soleil.nanoscopium.FXview.content;

import com.soleil.nanoscopium.FXview.content.label.ButtonContent;
import com.soleil.nanoscopium.FXview.module.PhaseReconstructionModule;
import com.soleil.nanoscopium.FXview.utils.FXutils;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * Created by bergamaschi on 10/11/2015.
 */
public class ReduceImageContentPane extends DefaultContentPane {



    @Override
    protected Node getContentPane() {

        //Create a TabPane View
        TabPane tabPane = new TabPane();


        Tab tabReduction = new Tab(FXutils.getLang("contentPane.ReduceImageContentPane.TabPane.reduce"));
        Tab tabVisualization = new Tab(FXutils.getLang("contentPane.ReduceImageContentPane.TabPane.visualization"));
        Tab tabCorrection = new Tab(FXutils.getLang("contentPane.ReduceImageContentPane.TabPane.correction"));

        tabVisualization.setClosable(false);
        tabReduction.setClosable(false);
        tabCorrection.setClosable(false);

        tabPane.getSelectionModel().selectedIndexProperty().addListener((l, j, k) -> {
            if ( k.intValue() == 0 ) {
                tabPane.getTabs().get(k.intValue()).setContent(createReductionPane());
            }
            else if ( k.intValue() == 1 ) {
                tabPane.getTabs().get(k.intValue()).setContent(createCorrectionPane());
            }
            else if ( k.intValue() == 2 ) {
                tabPane.getTabs().get(k.intValue()).setContent(createVisualizationPane());
            }
        });

        tabPane.getTabs().addAll(tabReduction,tabCorrection,tabVisualization);

        return tabPane;
    }

    private Pane createVisualizationPane(){
        GridPane gridPane = new GridPane();

        return gridPane;
    }

    private Pane createCorrectionPane(){
        GridPane gridPane = new GridPane();

        return gridPane;
    }

    private Pane createReductionPane(){
        GridPane gridPane = new GridPane();

//        final PhaseReconstructionModule phaseReconstructionModule = new PhaseReconstructionModule(R);

        //Get default View
        final AnchorPane defaultView = new AnchorPane();

        //Button to switch defaultView display
        ToggleGroup group = new ToggleGroup();

        ToggleButton selectReductionRoi = new ToggleButton(FXutils.getLang("contentPane.ReduceImageContentPane.TabPane.Reduction.selectROI"));
        ToggleButton darkFieldOption = new ToggleButton(FXutils.getLang("contentPane.ReduceImageContentPane.TabPane.Reduction.darkField"));
        ToggleButton phaseOption = new ToggleButton(FXutils.getLang("contentPane.ReduceImageContentPane.TabPane.Reduction.phaseOption"));

        Text text = GlyphsDude.createIcon(FontAwesomeIcon.CHECK_CIRCLE,FXutils.getResourceAsString("MMXI.icon.default.size"));
        text.getStyleClass().add("fontawesomeIconDefault");

        Separator separator = new Separator(Orientation.HORIZONTAL);
//        separator.
        selectReductionRoi.setToggleGroup(group);
        darkFieldOption.setToggleGroup(group);
        phaseOption.setToggleGroup(group);

        phaseOption.setOnAction(l->{
            PhaseReconstructionModule phaseReconstructionModule = new PhaseReconstructionModule(RXTomoJ.getInstance().getModelController().getDataController().getPhaseContrastInfo());
            defaultView.getChildren().clear();
            Pane pane = phaseReconstructionModule.getPane();

            AnchorPane.setTopAnchor(pane,0.0d);
            AnchorPane.setBottomAnchor(pane,0.0d);
            AnchorPane.setLeftAnchor(pane,0.0d);
            AnchorPane.setRightAnchor(pane,0.0d);

            defaultView.getChildren().add(pane);
        });

        selectReductionRoi.setSelected(true);

        GridPane.setConstraints(defaultView, 0, 0, 1, 4, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));
        GridPane.setConstraints(selectReductionRoi, 1, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.ALWAYS, new Insets(0, 0, 0, 0));
        GridPane.setConstraints(separator, 1, 1, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.ALWAYS, new Insets(0, 0, 0, 0));

        GridPane.setConstraints(darkFieldOption, 1, 2, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.ALWAYS, new Insets(0, 0, 0, 0));
        GridPane.setConstraints(phaseOption, 1, 3, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.ALWAYS, new Insets(0, 0, 0, 0));

        gridPane.getChildren().addAll(defaultView,separator,selectReductionRoi,darkFieldOption,phaseOption);

        return gridPane;
    }

    @Override
    protected ButtonContent getToggleButton() {
        Image default_icon = new Image(FXutils.getResource("MMXI.icon.default.fluo"));
        Image hover_icon = new Image(FXutils.getResource("MMXI.icon.hover.fluo"));
        Image selected_icon = new Image(FXutils.getResource("MMXI.icon.selected.fluo"));
        Image disabled_icon = new Image(FXutils.getResource("MMXI.icon.disabled.fluo"));

        ButtonContent fluo  = new ButtonContent("Fluorescence",default_icon,hover_icon,selected_icon,disabled_icon);
        return fluo;
    }

    @Override
    protected String getDefaultStatus() {
        return FXutils.getLang("contentPane.ReduceImageContentPane.defaultStatus");
    }

    @Override
    protected void modelEvent(MMXIControllerEvent mmxiControllerEvent) {

    }

    @Override
    void update() {
        System.err.println("Do nothing Yet");
    }
}
