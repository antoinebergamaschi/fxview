package com.soleil.nanoscopium.FXview.content;

import com.soleil.nanoscopium.FXview.accordion.AccordionPane;
import com.soleil.nanoscopium.FXview.accordion.OptionBar;
import com.soleil.nanoscopium.FXview.accordion.option.HistogramOption;
import com.soleil.nanoscopium.FXview.accordion.option.SpotRoiOption;
import com.soleil.nanoscopium.FXview.content.label.ButtonContent;
import com.soleil.nanoscopium.FXview.utils.FXutils;
import com.soleil.nanoscopium.FXview.utils.MMXISettings;
import com.soleil.nanoscopium.rximage.RXVirtualImage;
import com.soleil.nanoscopium.rximage.component.FX.RXImageCanvas;
import com.soleil.nanoscopium.rximage.component.FX.utils.lut.RXPixelColor;
import com.soleil.nanoscopium.rximage.component.RXImageComponentFX;
import com.soleil.nanoscopium.rximage.event.view.RXMouseChangedListener;
import com.soleil.nanoscopium.rximage.event.view.RXRoiChangedListener;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import com.soleil.nanoscopium.rxtomoj.model.data.ReductionInfo;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.util.converter.IntegerStringConverter;


/**
 * Created by bergamaschi on 12/10/2015.
 */
public class SpotContentPane extends DefaultContentPane {

    private ScrollBar scrollBar;
    private PositionPane positionPane;
    private SpotRoiOption roiPane;

    private RXImageComponentFX currentSpot;
    private RXImageComponentFX computedSpotMap;

    @Override
    protected String getDefaultStatus() {
        return FXutils.getLang("contentPane.SpotContentPane.defaultStatus");
    }

    @Override
    protected void modelEvent(MMXIControllerEvent mmxiControllerEvent) {
        System.out.println("No implemented L.27 "+this.getClass().toString());
    }


    @Override
    protected Node getContentPane() {
        AccordionPane accordionPane = new AccordionPane();


        GridPane gridPane = new GridPane();

        final RXVirtualImage rxVirtualImage = new RXVirtualImage(RXTomoJ.getInstance().getModelController().getIoController().openDefaultRawImage());
        currentSpot = new RXImageComponentFX(rxVirtualImage);
        currentSpot.setFilterFunction(RXTomoJ.getInstance().getModelController().getDataController().getFilterRawFunction(false));
        //Init the image and set the Default map color
        ((RXImageCanvas) currentSpot.getImage()).getPixelColor().setColorMap(MMXISettings.instance.getPref_color_map());
        roiPane = new SpotRoiOption(currentSpot);

        long[] dims = rxVirtualImage.getVirtualGlobalDimension();

        HBox v1 = new HBox();
        v1.setSpacing(20);
        Label spotTitle = new Label(RXTomoJ.getInstance().getModelController().getDataController().getDatasetName());
        Label spotDimension = new Label(FXutils.getLang("contentPane.SpotContentPane.label.spotDimension")+"\t("+dims[0]+","+dims[1]+","+dims[2]+")");
        v1.getChildren().addAll(spotTitle,spotDimension);
        v1.getStyleClass().add("spotContentLabel");

        HBox v3 = new HBox();
        v3.setSpacing(20);
        Label values = new Label();
        values.setMinWidth(50);

        //Add the Change to Onclik darkField
        CheckBox doDarkField = new CheckBox(FXutils.getLang("contentPane.SpotContentPane.checkbox.filterDarkField"));
        //Update the FilterWhen checkBox is clicked
        doDarkField.setOnAction(l->{
            currentSpot.setFilterFunction(RXTomoJ.getInstance().getModelController().getDataController().getFilterRawFunction(doDarkField.isSelected()));
        });

        //Add Object and Class style
        v3.getChildren().addAll(values,doDarkField);
        v3.getStyleClass().add("spotContentLabel");

        Pane image = currentSpot.getImage();

        HBox v2 = new HBox();
        Label spotMaskTitle = new Label(FXutils.getLang("contentPane.SpotContentPane.label.spotMap"));
        v2.getChildren().add(spotMaskTitle);
        v2.getStyleClass().add("spotContentLabel");
        spotMaskTitle.setTooltip(new Tooltip(FXutils.getLang("contentPane.SpotContentPane.spotMap.tooltips")));

        computedSpotMap = new RXImageComponentFX(RXTomoJ.getInstance().getModelController().getDataController().getComputedStack(ComputationUtils.StackType.SPOT_MAP));
        Pane spot = computedSpotMap.getImage();
        RXPixelColor pixelColor = ((RXImageCanvas) spot).getPixelColor();
        pixelColor.setColorMap(RXPixelColor.RXColorMap.SpotMapScale);

        scrollBar = new ScrollBar();
        scrollBar.setMax(rxVirtualImage.getVirtualSize());
        scrollBar.setMin(0);
        scrollBar.setUnitIncrement(1);
        scrollBar.setBlockIncrement(scrollBar.getUnitIncrement() * 10);
        scrollBar.setPrefHeight(50);

        positionPane = new PositionPane(rxVirtualImage.getVirtualGlobalDimension());


        scrollBar.valueProperty().addListener((observable, oldValue, newValue) -> {
            currentSpot.updateImage(newValue.intValue());

            //Update the position displayed
            double[] pos = ComputationUtils.convertHdfPositionToCartesianMap((int)rxVirtualImage.getVirtualGlobalDimension()[0],(int)rxVirtualImage.getVirtualGlobalDimension()[1],newValue.intValue());
            positionPane.update((int)pos[0],(int)pos[1],(int)pos[2]);
        });

        currentSpot.addListener((RXRoiChangedListener) roi ->{
            int[][] r = new int[2][2];
            r[0][0] = roi[0];
            r[0][1] = roi[0]+roi[2];
            r[1][0] = roi[1];
            r[1][1] = roi[1]+roi[3];

            RXTomoJ.getInstance().getModelController().getDataController().setRoiSpot(r);
        });

        //Update Values displayed
        currentSpot.addListener((RXMouseChangedListener) (v, p) -> values.setText( v + "[" + p[0] + "," + p[1] + "]"));

        accordionPane.addOption(OptionBar.OptionBarOrientation.HORIZONTAL, roiPane);
        accordionPane.addOption(OptionBar.OptionBarOrientation.HORIZONTAL, new HistogramOption((RXImageCanvas) currentSpot.getImage()));

        //Add the SpotMap
        GridPane.setConstraints(v1, 0, 0, 1, 1, HPos.LEFT, VPos.BOTTOM, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 0));
        GridPane.setConstraints(v3, 1, 0, 1, 1, HPos.RIGHT, VPos.BOTTOM, Priority.ALWAYS, Priority.NEVER, new Insets(0, 0, 0, 0));
        GridPane.setConstraints(image, 0, 1, 2, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));

        //Add the SpotMap Mask
        GridPane.setConstraints(v2, 0, 2, 2, 1, HPos.LEFT, VPos.BOTTOM, Priority.ALWAYS, Priority.NEVER, new Insets(10, 0, 0, 0));
        GridPane.setConstraints(spot, 0, 3, 2, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));


        GridPane.setConstraints(scrollBar, 0, 4, 2, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(10, 0, 0, 0));
        GridPane.setConstraints(positionPane, 0, 5, 2, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(20, 0, 0, 0));

        gridPane.getChildren().addAll(image,spot,v1,v2,v3,scrollBar,positionPane);

        accordionPane.setCenterContent(gridPane);

        return accordionPane;
    }

    @Override
    protected ButtonContent getToggleButton() {
        Image default_icon = new Image(FXutils.getResource("MMXI.icon.default.spot"));
        Image hover_icon = new Image(FXutils.getResource("MMXI.icon.hover.spot"));
        Image selected_icon = new Image(FXutils.getResource("MMXI.icon.selected.spot"));
        Image disabled_icon = new Image(FXutils.getResource("MMXI.icon.disabled.spot"));

        ButtonContent fluo  = new ButtonContent("Spot selection",default_icon,hover_icon,selected_icon,disabled_icon);
        return fluo;
    }

    @Override
    void update() {
        ReductionInfo reductionInfo = RXTomoJ.getInstance().getModelController().getDataController().getReductionInfo();
        int[][] roiSpot = reductionInfo.getTransmissionROI();
        int[]roi = new int[4];
        roi[0] = roiSpot[0][0];
        roi[1] = roiSpot[1][0];
        roi[2] = roiSpot[0][1]-roiSpot[0][0];
        roi[3] = roiSpot[1][1]-roiSpot[1][0];

        currentSpot.updateROI(roi);
        computedSpotMap.updateROI(roi);
    }

    /**
     * Position GridPane, display position in cartesian format, field may be fill to navigate in data
     */
    private class PositionPane extends GridPane{
        final private TextField xPosition = new TextField();
        final private TextField yPosition = new TextField();
        final private TextField thetaPosition = new TextField();
        final private double margin_left_right = 5;
        final private double margin_top_bottom = 10;

        private long[] maxDims;

        public PositionPane(final long[] maxDims){
            this.maxDims = maxDims;

            Label positionLabel = new Label(FXutils.getLang("contentPane.SpotContentPane.position.label"));

            TextFormatter<Integer> t = new TextFormatter<>(new IntegerStringConverter(),0);
            TextFormatter<Integer> t1 = new TextFormatter<>(new IntegerStringConverter(),0);
            TextFormatter<Integer> t2 = new TextFormatter<>(new IntegerStringConverter(),0);

//            TextField xPosition = new TextField();
            xPosition.setTextFormatter(t);
            Label xPositionLabel = new Label(FXutils.getLang("contentPane.SpotContentPane.position.x.label"));
            xPositionLabel.setLabelFor(xPosition);

//            TextField yPosition = new TextField();
            yPosition.setTextFormatter(t1);
            Label yPositionLabel = new Label(FXutils.getLang("contentPane.SpotContentPane.position.y.label"));
            yPositionLabel.setLabelFor(yPosition);

//            TextField thetaPosition = new TextField();
            thetaPosition.setTextFormatter(t2);
            Label thetaPositionLabel = new Label(FXutils.getLang("contentPane.SpotContentPane.position.theta.label"));
            thetaPositionLabel.setLabelFor(thetaPosition);

            //Add listener to Avoid going overrange
            xPosition.textProperty().addListener((observable,old,value)->{
                if ( Integer.parseInt(value) > (int)maxDims[0] ){
                    xPosition.setText(old);
                }else{
                    scrollBar.setValue(getPosition());
                }
            });

            yPosition.textProperty().addListener((observable,old,value)->{
                if ( Integer.parseInt(value) > (int)maxDims[1] ){
                    yPosition.setText(old);
                }else{
                    scrollBar.setValue(getPosition());
                }
            });

            thetaPosition.textProperty().addListener((observable,old,value)->{
                if ( Integer.parseInt(value) > (int)maxDims[2] ){
                    thetaPosition.setText(old);
                }else{
                    scrollBar.setValue(getPosition());
                }
            });

            GridPane.setConstraints(positionLabel, 0, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.ALWAYS, new Insets(0, 0, 0,  margin_left_right+5));

            GridPane.setConstraints(xPositionLabel, 1, 1, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.ALWAYS, new Insets(margin_top_bottom, 0, margin_top_bottom, margin_left_right));
            GridPane.setConstraints(xPosition, 2, 1, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(margin_top_bottom, margin_left_right, margin_top_bottom, 0));
            GridPane.setConstraints(yPositionLabel, 3, 1, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.ALWAYS, new Insets(margin_top_bottom, 0, margin_top_bottom, margin_left_right));
            GridPane.setConstraints(yPosition, 4, 1, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(margin_top_bottom, margin_left_right, margin_top_bottom, 0));
            GridPane.setConstraints(thetaPositionLabel, 5, 1, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.ALWAYS, new Insets(margin_top_bottom, 0, margin_top_bottom, margin_left_right));
            GridPane.setConstraints(thetaPosition, 6, 1, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(margin_top_bottom, margin_left_right+5, margin_top_bottom, 0));


            this.getChildren().addAll(positionLabel,xPositionLabel,xPosition,yPositionLabel,yPosition,thetaPosition,thetaPositionLabel);
        }

        private int getPosition(){
            int pos = Integer.parseInt(xPosition.getText());
            pos += Integer.parseInt(yPosition.getText())*maxDims[0];
            pos += Integer.parseInt(thetaPosition.getText())*maxDims[0]*maxDims[1];

            return pos;
        }

        public void update(int... newPosition){
            xPosition.setText(newPosition[0]+"");
            yPosition.setText(newPosition[1]+"");
            if ( newPosition.length > 2 ) {
                thetaPosition.setText(newPosition[2] + "");
            }
        }
    }
}
