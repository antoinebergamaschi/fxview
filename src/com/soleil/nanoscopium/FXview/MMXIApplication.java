package com.soleil.nanoscopium.FXview;

import com.soleil.nanoscopium.FXview.content.*;
import com.soleil.nanoscopium.FXview.utils.generic.DialogLoadHdf5;
import com.soleil.nanoscopium.FXview.utils.generic.DialogSaveLoadSession;
import com.soleil.nanoscopium.FXview.utils.FXutils;
import com.soleil.nanoscopium.FXview.utils.MMXISettings;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.ModelController;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import com.soleil.nanoscopium.rxtomoj.event.MMXIEventData;
import com.soleil.nanoscopium.rxtomoj.model.SessionObject;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.NotificationPane;
import org.controlsfx.control.Notifications;
import org.controlsfx.control.StatusBar;

/**
 * Created by antoine bergamaschi on 13/04/2015.
 * Construct the XXMI primary stage
 */
public class MMXIApplication extends Application{

    private BorderPane content;
    private static ObservableList<DefaultContentPane> panes = FXCollections.observableArrayList();
    private StatusBar statusBar;
    private Notifications notificationBuilder;
    private NotificationPane notificationPane;


    public static void main(String[] args) {
        //Add every interface in the interface
        DefaultContentPane defaultContentPane = new DefaultContent();
        DefaultContentPane select = new DataSelectionContentPane();
        DefaultContentPane fl = new FluoContentPane();
        DefaultContentPane re = new ReconstructionContentPane();
        DefaultContentPane sc = new SpotContentPane();
        DefaultContentPane ri = new ReduceImageContentPane();

        MMXIApplication.addContentPane(defaultContentPane);
        MMXIApplication.addContentPane(select);
        MMXIApplication.addContentPane(sc);
        MMXIApplication.addContentPane(fl);
        MMXIApplication.addContentPane(ri);
        MMXIApplication.addContentPane(re);

        //Open Settings
        MMXISettings.open();

        //Launch application
        launch(args);
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        Scene scene = new Scene(buildRoot(primaryStage));
        scene.getStylesheets().add(FXutils.getResourceAsString("MMXI.css.default"));

        primaryStage.setMinWidth(800);
        primaryStage.setMinHeight(800);

        primaryStage.setScene(scene);
        primaryStage.show();


        primaryStage.setOnCloseRequest(l->{
            //Ask for saving the SessionObject if there is one
            if ( RXTomoJ.getInstance().getModelController().getSessionController().isSessionOpen() ){
                DialogSaveLoadSession.saveBeforeClose(primaryStage);
            }

            //Save settings
            MMXISettings.save();
        });

        panes.addListener((ListChangeListener<ContentPane>) c -> {
            reload();
        });

        //Set Display for debug only

//        updateContentDisplay(true, false, ReduceImageContentPane.class.getName());
//        setSelected(true,ReduceImageContentPane.class.getName());

        //Link the application with the Model event
        RXTomoJ.getInstance().getModelController().addMMXIControllerUpdatedListener(l -> {
            //Display on screen everything happening
            System.out.println(l.toString());

            //Get the EventData
            MMXIEventData eventData = l.getEventData();
            if ( l.getEventType() == MMXIControllerEvent.MMXIControllerEventType.ERROR ){
                notificationBuilder.text(eventData.getMsg())
                        .title("Error")
                        .showError();
                return;
            }
            else if ( l.getEventType() == MMXIControllerEvent.MMXIControllerEventType.WARNING ){
                notificationBuilder.text(eventData.getMsg())
                        .title("Warning")
                        .showWarning();
                return;
            }

            //Switch on the different MMXI events
            switch (l.getEventID()) {
                case SESSION_CLOSED:
                    enableDisablePane();
                    if (l.getEventType() == MMXIControllerEvent.MMXIControllerEventType.UPDATE) {
                        //Set Selected
                        setSelected(true,DefaultContent.class.getName());
                    }
                    break;
                case SESSION_OPENED:
                    enableDisablePane();
                    if (l.getEventType() == MMXIControllerEvent.MMXIControllerEventType.UPDATE) {
                        //Set Selected
                        setSelected(true,DefaultContent.class.getName());

                        //Edit the Settings file
                        MMXISettings.instance.setLast_openedSession(RXTomoJ.getInstance().getModelController().getSessionController().getSessionObject().getPathToSessionObject());
                    }
                    break;
                case HDF_OPENED:
                    enableDisablePane();
                    if (l.getEventType() == MMXIControllerEvent.MMXIControllerEventType.UPDATE) {
                        //Set Selected
                        setSelected(true,DataSelectionContentPane.class.getName());

                        //Edit the Settings file
                        MMXISettings.instance.setLast_openedHdf5(RXTomoJ.getInstance().getModelController().getSessionController().getPathToHdf5());

                        //Ask to Save the SessionObject

                    }
                    break;
                case FLUORESCENCE_CHANGED:
                    enableDisablePane();
                    break;
                case IMAGE_STACK_CHANGED:
                    enableDisablePane();
                    break;
                case INIT_DATASET:
                    enableDisablePane();
                    break;
                case PROGRESS_FUNCTION:
                    break;
                default:
                    enableDisablePane();
            }
        });
    }

    /**
     * This function is constantly called
     * Check in the Model if Any of the Current Pane have to be set enable / disable
     */
    private void enableDisablePane(){
        ModelController controller = RXTomoJ.getInstance().getModelController();

        if ( controller.getSessionController().isSessionOpen() &&
                controller.getSessionController().getPathToHdf5() != null ){
            updateContentDisplay(true,false,DataSelectionContentPane.class.getName());
        }else{
            updateContentDisplay(false,true,DataSelectionContentPane.class.getName());
        }
        //Test if an image has been entered, if the image is 2D and finally if the spotMap is initialized
        if (controller.getSessionController().getDataset(SessionObject.DataType.IMAGE).size() >  0 &&
                controller.getDataController().isIssueFrom2DDetector() &&
                controller.getDataController().isSpotInitialized() ) {
            //Enable the Transmission Modalities
            updateContentDisplay(true,false,SpotContentPane.class.getName());
        }else{
            updateContentDisplay(false,true,SpotContentPane.class.getName());
        }
        if ( controller.getSessionController().getDataset(SessionObject.DataType.SPECTRUM).size() >  0) {
            //Enable the Fluorescence Modality
            updateContentDisplay(true,false,FluoContentPane.class.getName());
        }else{
            updateContentDisplay(false,true,FluoContentPane.class.getName());
        }
        if ( controller.getDataController().hasImage() ){
            System.out.println("Test here if Image are projection are 3D or 2D");
            updateContentDisplay(true,false,ReconstructionContentPane.class.getName());
        }else{
            updateContentDisplay(false,true,ReconstructionContentPane.class.getName());
        }
    }

    /**
     * Simulate the selection of one of the DefaultContentPane Object
     * @param sel True or False if selected
     * @param className The String class name to select
     */
    private void setSelected(boolean sel, String className){
        for (DefaultContentPane defaultContent : panes) {
            if (defaultContent.getClass().getName() ==  className) {
                defaultContent.getButtonContent().setSelected(sel);
            }
        }
    }

    /**
     *
     * @param hightlight True to Hightlight
     * @param enable True to disable !
     * @param className The ContentPane class to select
     */
    private void updateContentDisplay(boolean hightlight, boolean enable, String className){
        for (DefaultContentPane defaultContent : panes) {
            if (defaultContent.getClass().getName() ==  className) {
                defaultContent.getButtonContent().setDisable(enable);
                if ( !defaultContent.getButtonContent().isDisabled() ) {
                    defaultContent.getButtonContent().setIsHightlighted(hightlight);
                }
            }
        }
    }

    private AnchorPane buildRoot(Stage stage){
        AnchorPane  root = new AnchorPane();

        content = buildBorderPane();
        //Warning Content must be created to be called here
        setupMenu(stage);

        //Create Notification Builder
        notificationBuilder = Notifications.create()
                .hideAfter(Duration.INDEFINITE)
                .position(Pos.CENTER)
                .owner(stage);

        AnchorPane.setTopAnchor(content, 0.0d);
        AnchorPane.setBottomAnchor(content, 0.0d);
        AnchorPane.setLeftAnchor(content, 0.0d);
        AnchorPane.setRightAnchor(content, 0.0d);

        root.getChildren().add(content);

        return root;
    }

    private BorderPane buildBorderPane(){
        BorderPane pane = new BorderPane();
        //Add An Achor to stick the Gri
        AnchorPane stickLeftGrid = new AnchorPane();
        //No top
        //Set menuBar on TOP
//        MenuBar menuBar = setupMenu();

        GridPane gridPane =  new GridPane();
        gridPane.getStyleClass().addAll("leftPanel", "borderDefault","contentDefault");

        ToggleGroup toggleGroup = new ToggleGroup();

        AnchorPane stick = new AnchorPane();
        //Create new NotificationPane
        notificationPane = new NotificationPane(content);
        //Display on Top
        notificationPane.setShowFromTop(true);
        //Anchor the NotificationPane on each corner
        AnchorPane.setRightAnchor(notificationPane, 0.0);
        AnchorPane.setLeftAnchor(notificationPane, 0.0);
        AnchorPane.setTopAnchor(notificationPane, 0.0);
        AnchorPane.setBottomAnchor(notificationPane, 0.0);
        //Add the notificationPane in the AnchorPane
        stick.getChildren().add(notificationPane);
        //Add Styles
        stick.getStyleClass().addAll("contentDefault", "borderDefault");
        //Set center
        pane.setCenter(stick);

        int i = 0;
        for ( DefaultContentPane defaultContent : panes ){
            ToggleButton toggleButton = defaultContent.getButton();
            if ( !(defaultContent instanceof DefaultContent) ){
                defaultContent.getButtonContent().setDisable(true);
            }

            GridPane.setConstraints(toggleButton, 0, i, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 0));
            toggleButton.setToggleGroup(toggleGroup);


            //Add Listener
            toggleButton.selectedProperty().addListener(k -> {
                if ( toggleButton.isSelected() ) {
                    Node content = defaultContent.getContent();
                    setCenter(content);
                }
            });

            defaultContent.addStatusListener((text, progress) -> {
                if (statusBar != null) {
                    statusBar.setText(text);
                    if (progress >= 0) {
                        statusBar.setProgress(progress);
                    }else{
                        statusBar.setProgress(0.0);
                    }
                }
            });

            defaultContent.addNotificationListener(text -> {
                notificationPane.setText(text);
                notificationPane.show();
            });

            defaultContent.addErrorListener(text -> {
                notificationBuilder.text(text)
                        .title("Error")
                        .showError();
            });

            //Add a listener by interface
            RXTomoJ.getInstance().getModelController().addMMXIControllerUpdatedListener(defaultContent);

            gridPane.getChildren().add(toggleButton);
            i++;
        }

        AnchorPane.setTopAnchor(gridPane, 0.0d);
        AnchorPane.setBottomAnchor(gridPane, 0.0d);
        AnchorPane.setLeftAnchor(gridPane, 0.0d);
        AnchorPane.setRightAnchor(gridPane, 0.0d);

        stickLeftGrid.getChildren().add(gridPane);

        pane.setLeft(stickLeftGrid);
//        pane.setTop(menuBar);


        statusBar = new StatusBar();
        statusBar.setText("");

        pane.setBottom(statusBar);

        toggleGroup.selectToggle(toggleGroup.getToggles().get(0));

        return pane;
    }


    private void setupMenu(Stage stage){
        MenuBar menuBar = new MenuBar();

        Menu menu = new Menu(FXutils.getLang("menu.option"));
        Menu menu1 = new Menu(FXutils.getLang("menu.option.open"));
        MenuItem saveItem = new MenuItem(FXutils.getLang("menu.option.save"));
//        MenuItem resetItem = new MenuItem(FXutils.getLang("menu.option.reset"));
        MenuItem newItem = new MenuItem(FXutils.getLang("menu.option.new"));

        menu.getItems().add(newItem);
        menu.getItems().add(menu1);
        menu.getItems().add(saveItem);
//        menu.getItems().add(resetItem);

        MenuItem openSessionItem = new MenuItem(FXutils.getLang("menu.option.open.session"));
        MenuItem openHdfItem = new MenuItem(FXutils.getLang("menu.option.open.hdf5"));

        menu1.getItems().add(openSessionItem);
        menu1.getItems().add(openHdfItem);


        menuBar.getMenus().add(menu);

        newItem.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.SHORTCUT_DOWN));
        saveItem.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN));
        openSessionItem.setAccelerator(new KeyCodeCombination(KeyCode.R, KeyCombination.SHORTCUT_DOWN));
        openHdfItem.setAccelerator(new KeyCodeCombination(KeyCode.F, KeyCombination.SHORTCUT_DOWN));

        //Add Listener
        saveItem.setOnAction(l -> DialogSaveLoadSession.saveSession(stage));

//        resetItem.setOnAction(l -> RXTomoJ.getInstance().getModelController().getSessionController().resetSessionObject());

        openSessionItem.setOnAction(l -> DialogSaveLoadSession.loadSession(stage));

        openHdfItem.setOnAction(l -> DialogLoadHdf5.loadHdf5(stage));

        //Open a new Session
        newItem.setOnAction(l-> DialogSaveLoadSession.newSession(stage));

        content.setTop(menuBar);
    }


    public static void addContentPane(DefaultContentPane toAdd){
        panes.add(toAdd);
    }

    private void reload(){
        content = buildBorderPane();
    }

    private void setCenter(Node content){
        notificationPane.setContent(content);
    }
}
