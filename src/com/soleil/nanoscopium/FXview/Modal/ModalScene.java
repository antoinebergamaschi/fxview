package com.soleil.nanoscopium.FXview.Modal;

import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;

/**
 * Created by bergamaschi on 08/04/2015.
 */
public class ModalScene extends Scene {

    double dx;
    double dy;
    double initX;
    double initY;
    double deltaX;
    double deltaY;
    double borderTop = 30;
    double border = 10;
    boolean moveH;
    boolean moveV;
    boolean resizeH = false;
    boolean resizeV = false;
    boolean lockMoving = false;

    public ModalScene(Parent root) {
        super(root);
    }

    public ModalScene(Parent root, double width, double height) {
        super(root, width, height);
    }

    public ModalScene(Parent root, Paint fill) {
        super(root, fill);
    }

    public ModalScene(Parent root, double width, double height, Paint fill) {
        super(root, width, height, fill);
    }

    public ModalScene(Parent root, double width, double height, boolean depthBuffer) {
        super(root, width, height, depthBuffer);
    }

    public ModalScene(Parent root, double width, double height, boolean depthBuffer, SceneAntialiasing antiAliasing) {
        super(root, width, height, depthBuffer, antiAliasing);
    }


    public void initListener(Stage stage){
        this.setOnMouseMoved(l->{
            if ( stage.isResizable() ){
                if(l.getY() < borderTop){
                    this.setCursor(Cursor.DEFAULT);
                    lockMoving = true;
                    resizeH = false;
                    resizeV = false;
                    moveH = false;
                    moveV = false;
                }
                else if(l.getX() < border && l.getY() > this.getHeight() -border){
                    this.setCursor(Cursor.SW_RESIZE);
                    resizeH = true;
                    resizeV = true;
                    moveH = true;
                    moveV = false;
                    lockMoving = false;
                }
                else if(l.getX() > this.getWidth() -borderTop && l.getY() < borderTop){
                    this.setCursor(Cursor.DEFAULT);
                    lockMoving = true;
                    resizeH = false;
                    resizeV = false;
                    moveH = false;
                    moveV = false;
                }
                else if(l.getX() > this.getWidth() -border && l.getY() > this.getHeight() -border){
                    this.setCursor(Cursor.SE_RESIZE);
                    resizeH = true;
                    resizeV = true;
                    moveH = false;
                    moveV = false;
                    lockMoving = false;
                }
                else if(l.getX() < border || l.getX() > this.getWidth() -border){
                    this.setCursor(Cursor.E_RESIZE);
                    resizeH = true;
                    resizeV = false;
                    moveH = (l.getX() < border);
                    moveV = false;
                    lockMoving = false;
                }
                else if(l.getY() > this.getHeight() - border){
                    this.setCursor(Cursor.S_RESIZE);
                    lockMoving = false;
                    resizeH = false;
                    resizeV = true;
                    moveH = false;
                    moveV = false;
                }
                else{
                    this.setCursor(Cursor.DEFAULT);
                    resizeH = false;
                    resizeV = false;
                    moveH = false;
                    moveV = false;
                    lockMoving = false;
                }
            }
            else {
                if (l.getY() < borderTop) {
                    this.setCursor(Cursor.DEFAULT);
                    lockMoving = true;
                    resizeH = false;
                    resizeV = false;
                    moveH = false;
                    moveV = false;
                } else if (l.getX() > this.getWidth() - borderTop && l.getY() < borderTop) {
                    this.setCursor(Cursor.DEFAULT);
                    lockMoving = true;
                    resizeH = false;
                    resizeV = false;
                    moveH = false;
                    moveV = false;
                } else {
                    this.setCursor(Cursor.DEFAULT);
                    resizeH = false;
                    resizeV = false;
                    moveH = false;
                    moveV = false;
                    lockMoving = false;
                }
            }

        });
        this.setOnMousePressed(l->{
            dx = stage.getWidth() - l.getX();
            dy = stage.getHeight() - l.getY();
            initX = l.getScreenX() - stage.getX();
            initY = l.getScreenY() - stage.getY();
        });
        this.setOnMouseDragged(l -> {
            if (resizeH) {
                if (stage.getWidth() <= stage.getMinWidth()) {
                    if (moveH) {
                        deltaX = stage.getX() - l.getScreenX();
                        if (l.getX() < 0) {// if new > old, it's permitted
                            stage.setWidth(deltaX + stage.getWidth());
                            stage.setX(l.getScreenX());
                        }
                    } else {
                        if (l.getX() + dx - stage.getWidth() > 0) {
                            stage.setWidth(l.getX() + dx);
                        }
                    }
                } else if (stage.getWidth() > stage.getMinWidth()) {
                    if (moveH) {
                        deltaX = stage.getX() - l.getScreenX();
                        stage.setWidth(deltaX + stage.getWidth());
                        stage.setX(l.getScreenX());
                    } else {
                        stage.setWidth(l.getX() + dx);
                    }
                }
            } else if (resizeV) {
                if (stage.getHeight() <= stage.getMinHeight()) {
                    if (moveV) {
                        deltaY = stage.getY() - l.getScreenY();
                        if (l.getY() < 0) {// if new > old, it's permitted
                            stage.setHeight(deltaY + stage.getHeight());
                            stage.setY(l.getScreenY());
                        }
                    } else {
                        if (l.getY() + dy - stage.getHeight() > 0) {
                            stage.setHeight(l.getY() + dy);
                        }
                    }
                } else if (stage.getHeight() > stage.getMinHeight()) {
                    if (moveV) {
                        deltaY = stage.getY() - l.getScreenY();
                        stage.setHeight(deltaY + stage.getHeight());
                        stage.setY(l.getScreenY());
                    } else {
                        stage.setHeight(l.getY() + dy);
                    }
                }
            } else if (lockMoving) {
                stage.setX(l.getScreenX() - initX);
                stage.setY(l.getScreenY() - initY);
            }
        });

    }

}
