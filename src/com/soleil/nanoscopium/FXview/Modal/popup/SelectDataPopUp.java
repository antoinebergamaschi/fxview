package com.soleil.nanoscopium.FXview.Modal.popup;

import com.soleil.nanoscopium.FXview.utils.FXutils;
import com.soleil.nanoscopium.hdf5Opener.Hdf5Handler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by bergamaschi on 27/04/2015.
 */
public class SelectDataPopUp extends PopUp<ArrayList<String>> {

    private ListView<String> listView;
    private AnchorPane description;
    private HashMap<String, Hdf5Handler.Dataset_Iterate_cb_o1.DatasetObject> dataInfo;

    public SelectDataPopUp(ArrayList<String> containedValues,HashMap<String, Hdf5Handler.Dataset_Iterate_cb_o1.DatasetObject> hashMap){
        super();

        dataInfo = hashMap;
        //Init List
        for ( String key : hashMap.keySet() ){
            if ( !containedValues.contains(key) ){
                listView.getItems().add(key);
            }
        }

        listView.getSelectionModel().selectedItemProperty().addListener((ov, old_val, new_val) -> {
            description.getChildren().clear();
            Hdf5Handler.Dataset_Iterate_cb_o1.DatasetObject d = hashMap.get(listView.getSelectionModel().getSelectedItem());
            if ( d == null ){
                System.err.println("Test");
            }

            Node node = buildDescription(d);
            AnchorPane.setBottomAnchor(node,0.0d);
            AnchorPane.setLeftAnchor(node, 0.0d);
            AnchorPane.setTopAnchor(node, 0.0d);
            AnchorPane.setRightAnchor(node, 0.0d);

            description.getChildren().add(node);
        });

        //Sort in function of the rank
        listView.getItems().sort((o1,o2)-> dataInfo.get(o2).getRank() -  dataInfo.get(o1).getRank());

        this.setWidth(600);

        //Show and Wait for input
        this.showAndWait();
    }


    private Node buildDescription(Hdf5Handler.Dataset_Iterate_cb_o1.DatasetObject d){
        GridPane msg = new GridPane();
        Label name = new Label();
        if ( d.getName() != null ) {
            name.setText(d.getName());
        }else{
            name.setText("null");
        }

        GridPane.setConstraints(name, 0, 0, 2, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(5, 5, 5, 5));
        msg.getChildren().addAll(name);

        int i = 1;
        for ( String key : d.getAttributes().keySet() ){
            Label attributeName = new Label(key);
            Label attributeVal = new Label(d.getAttributes().get(key));
            attributeName.setTextOverrun(OverrunStyle.ELLIPSIS);
            attributeVal.setTextOverrun(OverrunStyle.ELLIPSIS);
            GridPane.setConstraints(attributeName, 0, i, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(5, 0, 5, 5));
            GridPane.setConstraints(attributeVal, 1, i, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(5, 5, 5, 0));
            i++;
            msg.getChildren().addAll(attributeName,attributeVal);
        }

        return msg;
    }

    @Override
    Node buildContent() {

        GridPane gridPane = new GridPane();
        Label title = new Label("Description");
        description = new AnchorPane();

        listView = new ListView<>();
        listView.setCellFactory(list -> new DataSetNames());
        listView.setMaxWidth(200);
        listView.setMinWidth(200);

        listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        GridPane.setConstraints(listView, 0, 0, 1, 2, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(5, 5, 5, 5));
        GridPane.setConstraints(title, 1, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(5, 5, 5, 5));
        GridPane.setConstraints(description, 1, 1, 1, 1, HPos.RIGHT, VPos.TOP, Priority.ALWAYS, Priority.ALWAYS, new Insets(5, 5, 5, 5));

        gridPane.getChildren().addAll(listView,title, description);

        return gridPane;
    }

    @Override
    Node buildTop() {
        return null;
    }

    @Override
    Node buildBottom() {
        Button val = new Button(FXutils.getLang("validate"));
        Button cancel = new Button(FXutils.getLang("cancel"));
        Button reset = new Button(FXutils.getLang("reset"));

        GridPane gridPane = new GridPane();
        GridPane.setConstraints(val, 0, 0, 1, 1, HPos.RIGHT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(0, 5, 0, 0));
        GridPane.setConstraints(cancel, 1, 0, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 5));
        GridPane.setConstraints(reset, 2, 0, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(0, 0, 0, 5));

        gridPane.getChildren().addAll(val, cancel, reset);

        gridPane.setAlignment(Pos.CENTER);

        cancel.setOnAction(l -> {
            data = new ArrayList<>();
            isCancel = true;
            this.close();
        });

        /**
         * Init The data which can be retrieved by the popUp controller
         */
        val.setOnAction(l->{
            data = new ArrayList<>(listView.getSelectionModel().getSelectedItems());
            isValid = true;
            this.close();
        });

        reset.setOnAction(l->{
            data = new ArrayList<>();
            isReset = true;
            this.close();
        });

        return gridPane;
    }

    @Override
    Node buildLeft() {
        return null;
    }

    @Override
    Node buildRight() {
        return null;
    }


    private class DataSetNames extends ListCell<String> {

        public DataSetNames() {    }

        @Override protected void updateItem(String item, boolean empty) {
            // calling super here is very important - don't skip this!
            super.updateItem(item, empty);

            if ( item != null ){
                setTooltip(new Tooltip(item));
                setText(item.substring(item.lastIndexOf("/")+1));
//                set;
//                if (dataInfo.get(item).getRank() == 1 ){
//                    setTextFill(Color.AQUA);
//                }else{
//                    setTextFill(Color.BLACK);
//                }
            }
        }
    }
}
