package com.soleil.nanoscopium.FXview.Modal.popup;

import com.soleil.nanoscopium.FXview.utils.FXutils;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Created by bergamaschi on 27/04/2015.
 */
abstract class PopUp<T> extends Stage {
    protected BorderPane content;

    protected boolean isCancel = false;
    protected boolean isReset = false;
    protected boolean isValid = false;

    protected T data;

    public PopUp(){
        super(StageStyle.UTILITY);

        content = new BorderPane();
        content.getStyleClass().add("popup");

        this.setMinHeight(200);
        this.setMinWidth(200);
        this.setScene(new Scene(content));

        Node node = this.buildContent();
        if ( node != null ){
            node.getStyleClass().add("center");
            content.setCenter(node);
        }

        node = this.buildBottom();
        if ( node != null ){
            node.getStyleClass().add("bottom");
            content.setBottom(node);
        }

        node = this.buildLeft();
        if ( node != null){
            content.setLeft(node);
        }

        node = this.buildRight();
        if ( node != null) {
            content.setRight(node);
        }

        node = this.buildTop();
        if ( node != null){
            content.setTop(node);
        }

        this.getScene().getStylesheets().add(FXutils.getResourceAsString("MMXI.css.popup"));
    }

    abstract Node buildContent();
    abstract Node buildTop();
    abstract Node buildBottom();
    abstract Node buildLeft();
    abstract Node buildRight();

    public T getData(){
        return data;
    }


    public boolean isCancel() {
        return isCancel;
    }

    public boolean isReset() {
        return isReset;
    }

    public boolean isValid() {
        return isValid;
    }
}
