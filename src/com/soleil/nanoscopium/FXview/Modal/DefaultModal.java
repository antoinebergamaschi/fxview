package com.soleil.nanoscopium.FXview.Modal;

import com.soleil.nanoscopium.FXview.utils.FXutils;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Created by bergamaschi on 23/10/2015.
 */
public class DefaultModal extends Stage {
    private Node modalContent;
    private Node modalBottom;
    private Node modalTop;

    private double modalDimX=2000;
    private double modalDimY=2000;

    public DefaultModal(){
        this(200, 200);
    }

    public DefaultModal(double initDimX, double initDimY){
        super(StageStyle.UTILITY);
        this.modalDimX = initDimX;
        this.modalDimY = initDimY;

        AnchorPane root = build();

        this.setMinHeight(modalDimY);
        this.setMinWidth(modalDimX);
//        this.setResizable(true);

        ModalScene scene  = new ModalScene(root);
        scene.initListener(this);
        scene.setFill(Color.TRANSPARENT);
        this.setScene(scene);

        scene.getStylesheets().add(FXutils.getResourceAsString("FXcss.modal.default"));
        this.sizeToScene();
    }

    private AnchorPane build(){
        AnchorPane root = new AnchorPane();
        BorderPane pane = new BorderPane();
        root.getStyleClass().add("contentModal");


        root.setPrefSize(modalDimX, modalDimY);
        AnchorPane.setBottomAnchor(pane, 0.0d);
        AnchorPane.setTopAnchor(pane, 0.0d);
        AnchorPane.setRightAnchor(pane, 0.0d);
        AnchorPane.setLeftAnchor(pane, 0.0d);
        root.getChildren().add(pane);

        modalTop =  new AnchorPane();
        pane.setTop(modalTop);


        modalContent =  new AnchorPane();
        modalContent.getStyleClass().add("bodyModal");
        AnchorPane anchorTop  = new AnchorPane();
        anchorTop.setPrefSize(0, 10);
        AnchorPane.setBottomAnchor(modalContent, 0.0d);
        AnchorPane.setLeftAnchor(modalContent, 0.0d);
        AnchorPane.setTopAnchor(modalContent, 0.0d);
        AnchorPane.setRightAnchor(modalContent, 0.0d);
        anchorTop.getChildren().add(modalContent);


        modalBottom  =  new AnchorPane();
        AnchorPane anchorBottom  = new AnchorPane();
        anchorBottom.setPrefSize(0, 10);
        AnchorPane.setBottomAnchor(modalBottom, 0.0d);
        AnchorPane.setLeftAnchor(modalBottom, 0.0d);
        AnchorPane.setTopAnchor(modalBottom, 0.0d);
        AnchorPane.setRightAnchor(modalBottom, 0.0d);
        anchorBottom.getChildren().add(modalBottom);

        modalBottom.getStyleClass().add("bottomModal");
        modalBottom.getStyleClass().add("empty");

        pane.setCenter(anchorTop);
        pane.setBottom(anchorBottom);


        modalContent.setOnMousePressed(e -> {
            //Destroy event
            e.consume();
        });

        modalContent.setOnMouseDragged(e->{
            //Destroy event
            e.consume();
        });

        return root;
    }

    public void setBody(Node node){
        //Empty the current ModalBody
        ((AnchorPane)modalContent).getChildren().clear();
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setFitToWidth(true);
        scrollPane.setFitToHeight(true);
        scrollPane.setClip(node.getClip());

        AnchorPane.setBottomAnchor(scrollPane, 0.0d);
        AnchorPane.setLeftAnchor(scrollPane, 0.0d);
        AnchorPane.setTopAnchor(scrollPane, 0.0d);
        AnchorPane.setRightAnchor(scrollPane, 0.0d);

        ((AnchorPane) modalContent).getChildren().add(scrollPane);

        scrollPane.setContent(node);
    }

    public void setModalBottom(Node node){
        //Empty the current ModalBody
        ((AnchorPane)modalBottom).getChildren().clear();

        AnchorPane.setBottomAnchor(node, 0.0d);
        AnchorPane.setLeftAnchor(node, 0.0d);
        AnchorPane.setTopAnchor(node, 0.0d);
        AnchorPane.setRightAnchor(node, 0.0d);

        ((AnchorPane) modalBottom).getChildren().add(node);

        modalBottom.getStyleClass().remove("empty");
    }

    public void setModalTop(Node node){
        //Empty the current ModalBody
        ((AnchorPane)modalTop).getChildren().clear();

        AnchorPane.setBottomAnchor(node, 0.0d);
        AnchorPane.setLeftAnchor(node, 0.0d);
        AnchorPane.setTopAnchor(node, 0.0d);
        AnchorPane.setRightAnchor(node, 0.0d);

        ((AnchorPane) modalTop).getChildren().add(node);

        modalTop.getStyleClass().remove("empty");
    }
}
