package com.soleil.nanoscopium.FXview.module;

import com.soleil.nanoscopium.FXview.utils.FXutils;
import com.soleil.nanoscopium.FXview.utils.generic.GenericInterfaceParameters;
import com.soleil.nanoscopium.rxtomoj.model.ShiftImage;
import com.soleil.nanoscopium.rxtomoj.model.computationFunction.ComputePhaseContrast;
import com.soleil.nanoscopium.rxtomoj.model.data.PhaseContrastInfo;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;

/**
 * Created by bergamaschi on 13/11/2015.
 */
public class PhaseReconstructionModule extends GenericInterfaceParameters<PhaseContrastInfo> {

    final ChoiceBox<ComputePhaseContrast.PhaseRetrievalAlgo> chooseAlgo = new ChoiceBox<>();
    final ChoiceBox<ComputePhaseContrast.PhaseComputationTechniques> chooseCompute = new ChoiceBox<>();

    final CheckBox normalizeX = new CheckBox(FXutils.getLang("module.phaseReconstructionModule.checkbox.normalizeX"));
    final CheckBox normalizeY = new CheckBox(FXutils.getLang("module.phaseReconstructionModule.checkbox.normalizeY"));

    final CheckBox useMirroring = new CheckBox(FXutils.getLang("module.phaseReconstructionModule.checkbox.mirroring"));

    final ChoiceBox<ShiftImage.MirroringType> chooseMirror = new ChoiceBox<>();

    final Label chooseMirroringLabel = new Label(FXutils.getLang("module.phaseReconstructionModule.label.chooseMirroring"));

    public PhaseReconstructionModule(PhaseContrastInfo parameterObject) {
        super(parameterObject);
    }

    @Override
    public Pane getPane() {

        GridPane pane = new GridPane();

        //Create the ChoiceBox to choose Algorithm
        chooseAlgo.getItems().addAll(ComputePhaseContrast.PhaseRetrievalAlgo.values());
        chooseAlgo.getSelectionModel().select(parameterObject.getPhaseRetrievalAlgo());
        //Test if Fourier is selected use Mirroring
        chooseAlgo.setOnAction(l->{
            boolean bol = !chooseAlgo.getSelectionModel().getSelectedItem().isFourrier();
            chooseMirroringLabel.setVisible(bol);
            useMirroring.setVisible(bol);
            chooseMirror.setVisible(bol);
        });

        //Choose how to compute the center of gravity
        chooseCompute.getItems().addAll(ComputePhaseContrast.PhaseComputationTechniques.values());
        chooseCompute.getSelectionModel().select(parameterObject.getPhaseComputationTechniques());

        //Normalize Data in X/Y direction
        normalizeX.setSelected(parameterObject.isNormalizeX());
        normalizeY.setSelected(parameterObject.isNormalizeY());

        //Depending on the algo select the options
        useMirroring.setSelected(parameterObject.isUseMirroring());
        useMirroring.setOnAction(l -> chooseMirror.setDisable(!useMirroring.isSelected()));

        //Choose the mirroring Option
        chooseMirror.getItems().addAll(ShiftImage.MirroringType.values());
        chooseMirror.getSelectionModel().select(parameterObject.getReal());
        chooseMirror.setDisable(!useMirroring.isSelected());

        Label chooseAlgoLabel = new Label(FXutils.getLang("module.phaseReconstructionModule.label.chooseAlgo"));
        Label chooseComputeLabel = new Label(FXutils.getLang("module.phaseReconstructionModule.label.chooseCompute"));


        GridPane.setConstraints(chooseAlgoLabel, 0, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));
        GridPane.setConstraints(chooseAlgo, 1, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));

        GridPane.setConstraints(chooseComputeLabel, 2, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));
        GridPane.setConstraints(chooseCompute, 3, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));

        GridPane.setConstraints(normalizeX, 0, 1, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));
        GridPane.setConstraints(normalizeY, 1, 1, 3, 1, HPos.LEFT, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));

        GridPane.setConstraints(chooseMirroringLabel, 0, 2, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));
        GridPane.setConstraints(useMirroring, 1, 2, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));
        GridPane.setConstraints(chooseMirror, 2, 2, 2, 1, HPos.LEFT, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS, new Insets(0, 0, 0, 0));


        pane.getChildren().addAll(chooseAlgo,chooseCompute,normalizeX,normalizeY,
                chooseAlgoLabel,chooseComputeLabel,useMirroring,chooseMirror,chooseMirroringLabel);

        return pane;
    }

    @Override
    public void update(PhaseContrastInfo newParameterObject) {
        this.parameterObject = newParameterObject;

        useMirroring.setSelected(parameterObject.isUseMirroring());

        normalizeX.setSelected(parameterObject.isNormalizeX());
        normalizeY.setSelected(parameterObject.isNormalizeY());

        chooseAlgo.getSelectionModel().select(parameterObject.getPhaseRetrievalAlgo());
        chooseCompute.getSelectionModel().select(parameterObject.getPhaseComputationTechniques());
        chooseMirror.getSelectionModel().select(parameterObject.getReal());


        boolean bol = !parameterObject.getPhaseRetrievalAlgo().isFourrier();
        chooseMirroringLabel.setVisible(bol);
        useMirroring.setVisible(bol);
        chooseMirror.setVisible(bol);
    }
}
