package com.soleil.nanoscopium.FXview.module;

import com.soleil.nanoscopium.FXview.utils.FXutils;
import com.soleil.nanoscopium.FXview.utils.generic.GenericInterfaceParameters;
import com.soleil.nanoscopium.rxtomoj.model.data.ReductionInfo;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;


/**
 * Created by bergamaschi on 12/11/2015.
 */
public class SpotDetectionModule extends GenericInterfaceParameters<ReductionInfo> {

    //TODO Float not working use Double instead
    final private Spinner<Integer> pixelSpacing = new Spinner(1,100,1);
    final private Spinner<Double> spotFilter = new Spinner(0.9,1,0.5);
    final private Spinner<Integer> minPixelNumber = new Spinner(1,100,1);
    final private Spinner<Double> minPixelValue = new Spinner(1.0,Float.MAX_VALUE,1.0);

    public SpotDetectionModule(ReductionInfo parameterObject) {
        super(parameterObject);
    }

    @Override
    public Pane getPane() {

        GridPane gridPane = new GridPane();

        //Define % of data to be readed 1 = min
        final Label pixelSpacingLabel = new Label(FXutils.getLang("module.spotDetectionModule.label.pixelSpacing"));
        pixelSpacingLabel.setLabelFor(pixelSpacing);
        pixelSpacing.getValueFactory().setValue(parameterObject.getPixelSpacing());
        final Label help1 = new Label("");

        Label spotFilterLabel = new Label(FXutils.getLang("module.spotDetectionModule.label.spotFilter"));
        spotFilterLabel.setLabelFor(spotFilter);
        spotFilter.getValueFactory().setValue((double) parameterObject.getSpotFilter());
        final Label help2 = new Label("");

        Label minPixelNumberLabel = new Label(FXutils.getLang("module.spotDetectionModule.label.pixelNumber"));
        minPixelNumberLabel.setLabelFor(minPixelNumber);
        minPixelNumber.getValueFactory().setValue(parameterObject.getMinimalPixelNumber());

        final Label help3 = new Label("");

        //Define % of restriction on pixel presence 1 = max
        Label minPixelValueLabel = new Label(FXutils.getLang("module.spotDetectionModule.label.pixelValue"));
        minPixelValueLabel.setLabelFor(minPixelValue);
        minPixelValue.getValueFactory().setValue((double) parameterObject.getMinimalPixelValue());
        final Label help4 = new Label("");

        GridPane.setConstraints(pixelSpacingLabel, 0, 0, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(marginOutside, marginInside, marginInside, marginOutside));
        GridPane.setConstraints(pixelSpacing, 1, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(marginOutside, marginInside, marginInside, marginInside));
        GridPane.setConstraints(help1, 2, 0, 1, 1, HPos.RIGHT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(marginOutside, marginOutside, marginInside, marginInside));

        GridPane.setConstraints(spotFilterLabel, 0, 1, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(marginInside, marginInside, marginInside, marginOutside));
        GridPane.setConstraints(spotFilter, 1, 1, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(marginInside, marginInside, marginInside, marginInside));
        GridPane.setConstraints(help2, 2, 1, 1, 1, HPos.RIGHT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(marginInside, marginOutside, marginInside, marginInside));

        GridPane.setConstraints(minPixelNumberLabel, 0, 2, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(marginInside, marginInside, marginInside, marginOutside));
        GridPane.setConstraints(minPixelNumber, 1, 2, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(marginInside, marginInside, marginInside, marginInside));
        GridPane.setConstraints(help3, 2, 2, 1, 1, HPos.RIGHT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(marginInside, marginOutside, marginInside, marginInside));

        GridPane.setConstraints(minPixelValueLabel, 0, 3, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(marginInside, marginInside, marginOutside, marginOutside));
        GridPane.setConstraints(minPixelValue, 1, 3, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER, new Insets(marginInside, marginInside, marginOutside, marginInside));
        GridPane.setConstraints(help4, 2, 3, 1, 1, HPos.RIGHT, VPos.CENTER, Priority.NEVER, Priority.NEVER, new Insets(marginInside, marginOutside, marginOutside, marginInside));

        gridPane.getChildren().addAll(pixelSpacingLabel,pixelSpacing,help1,spotFilterLabel,spotFilter,help2,minPixelNumberLabel,
                minPixelNumber,help3,minPixelValueLabel,minPixelValue,help4 );

        return gridPane;
    }

    @Override
    public void update(ReductionInfo newParameterObject) {
        parameterObject = newParameterObject;

        pixelSpacing.getValueFactory().setValue(parameterObject.getPixelSpacing());
        spotFilter.getValueFactory().setValue((double) parameterObject.getSpotFilter());
        minPixelNumber.getValueFactory().setValue(parameterObject.getMinimalPixelNumber());
        minPixelValue.getValueFactory().setValue((double) parameterObject.getMinimalPixelValue());
    }
}
