import javafx.application.Application;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import javafx.util.Callback;
import sample.FXButton;
import sample.FXModal;
import sample.FXutils;

/**
 * Created by antoine bergamaschi on 02/04/2015.
 */
public class ModalTest extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        testModal();
//        testGraphic();
    }


//    public static void main(String[] args) {
//        launch(args);
//    }


    public void testModal(){
        try {
            FXModal stage = new FXModal(500,600);
            AnchorPane borderPane = (AnchorPane) stage.getScene().lookup(".contentModal");
            TextFlow textFlow = new TextFlow();
            textFlow.getChildren().add(new Text("test"));

            TableView  tableView    =  createTable();

            CheckBox checkBox = new CheckBox();
            checkBox.setSelected(true);
            checkBox.setAllowIndeterminate(true);

            stage.setBody(tableView);

            stage.getScene().getStylesheets().clear();
            stage.getScene().getStylesheets().add("/FXcss/blackModal.css");
//            stage.setBody(checkBox);
//            ((AnchorPane)stage.getModalContent()).getChildren().add(tableView);
//            ((Pane) stage.getModalContent()).getChildren().add(textFlow);
//                    ((Pane) stage.getModalContent()).getChildren().add(tableView);

            //Test FileChooser
//            FileChooser fileChooser = new FileChooser();
//            fileChooser.showOpenDialog(stage);
//            JFileChooser jFileChooser = new JFileChooser();
//            jFileChooser.showOpenDialog(new JFrame("test"));


            stage.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void testGraphic(){
        try {
            FXModal stage = new FXModal(500,600);
            stage.setResizable(false);

            NumberAxis Xaxis = new NumberAxis(0,2048,256);
            NumberAxis Yaxis = new NumberAxis("Number of fluorescence Event",0,10,1);
            //defining a series
            XYChart.Series series = new XYChart.Series();

//  series.setName("My portfolio");
            //populating the series with data
            series.getData().add(new XYChart.Data(0, 0));
            series.getData().add(new XYChart.Data(256, 1));
            series.getData().add(new XYChart.Data(512, 2));
            series.getData().add(new XYChart.Data(700, 3));
            series.getData().add(new XYChart.Data(800, 4));
            series.getData().add(new XYChart.Data(900, 5));
            series.getData().add(new XYChart.Data(1000, 6));
            series.getData().add(new XYChart.Data(1100, 5));
            series.getData().add(new XYChart.Data(1200, 4));
            series.getData().add(new XYChart.Data(1300, 3));
            series.getData().add(new XYChart.Data(1400, 2));
            series.getData().add(new XYChart.Data(1500, 1));


            LineChart lineChart = new LineChart(Xaxis,Yaxis);
            lineChart.getData().add(series);
            lineChart.setLegendVisible(false);
            lineChart.setCreateSymbols(false);


            stage.setBody(lineChart);
            stage.show();

            stage.setModalTitle("TestY");
            stage.setModalTitle("TestX",new Image(FXutils.getRessource("logo.default")));
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public TableView createTable(){
        TableView tableView = new TableView();

        final ObservableList<TableObject> data = FXCollections.observableArrayList(
                new TableObject(true, "Area1"),
                new TableObject(false, "Area2"),
                new TableObject(true, "Area3"),
                new TableObject(true, "Area4"),
                new TableObject(false, "Area5"));



        TableColumn invitedCol = new TableColumn<String, Boolean>();
        invitedCol.setText("Taxon1");
        invitedCol.setMinWidth(50);
        invitedCol.setCellValueFactory(new PropertyValueFactory("isPresent"));
        invitedCol.setCellFactory(new Callback<TableColumn<?,?>, TableCell<?,?>>() {
            @Override public TableCell<?,?> call(TableColumn<?,?> param) {
                return new TableCell<Object,Object>() {
                    @Override protected void updateItem(Object item, boolean empty) {
                        //Nothing has been change
                        if (item == getItem()) return;

                        super.updateItem(item, empty);

                        if (item == null) {
                            super.setText(null);
                            super.setGraphic(null);
                        }
                        else if ( (boolean)item ){
                            FXButton fxButton = new FXButton("true","true");
                            fxButton.setIcon(FXButton.IconeButton.IMAGE_DEFAULT,"/image/valid_64_default.png");
                            super.setGraphic(fxButton);
                            super.setText(null);
                        }else{
                            FXButton fxButton = new FXButton("false","false");
                            fxButton.setIcon(FXButton.IconeButton.IMAGE_DEFAULT,"/image/remove_64_default.png");
                            super.setGraphic(fxButton);
                            super.setText(null);
                        }

//                        } else {
//                            super.setText(item.toString());
//                            super.setGraphic(null);
//                        }
                    }
                };
            }});

        //"Last Name" column
        TableColumn lastNameCol = new TableColumn();
        lastNameCol.setText("Area");
        lastNameCol.setCellValueFactory(new PropertyValueFactory("Area"));

        //Enabling editing
        tableView.setEditable(true);
        invitedCol.setEditable(true);
        tableView.getColumns().addAll(lastNameCol,invitedCol);

        tableView.setItems(data);
        return tableView;
    }

    //Person object
    public static class TableObject {
        private BooleanProperty isPresent;
        private StringProperty Area;
//        private StringProperty Taxon;

        private TableObject(boolean isPresent, String Area){
            this.Area = new SimpleStringProperty(Area);
//            this.Taxon = new SimpleStringProperty(Taxon);
            this.isPresent = new SimpleBooleanProperty(isPresent);

            this.isPresent.addListener(new ChangeListener<Boolean>() {
                public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                    System.out.println(AreaProperty().get() + " isPresent: " + t1);
                }
            });
        }

        public BooleanProperty isPresentProperty() { return isPresent; }

        public StringProperty AreaProperty() { return Area; }

//        public StringProperty TaxonProperty() { return Taxon; }

        public void setArea(String lastName) { this.Area.set(lastName); }

//        public void setTaxon(String firstName) { this.Taxon.set(firstName); }

        public void setisPresent(boolean isPresent) { this.isPresent.set(isPresent); }
    }
}
